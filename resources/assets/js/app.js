import './bootstrap.js';

//import Echo from 'laravel-echo';
import Vue from 'vue';

import AdminFile from './components/AdminFile';
import Feedback from './components/Feedback';
import InternetConnection from './components/InternetConnection';
import Jeopardy from './components/Jeopardy';

new Vue({

    el: '#dashboard',

    components: {
        AdminFile,
        Feedback,
        Jeopardy,
        InternetConnection,
    },
});
