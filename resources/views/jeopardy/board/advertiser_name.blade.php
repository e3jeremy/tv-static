<div class="name_wrap" data-color="{!! Board::advertiserColor() !!}" style="background-color: {!! Board::advertiserColor() !!}">
    <a href="#"
       class="points" id="points"
       data-original-title="Enter Points"
       data-type="text"
       data-pk="{!! Board::getAdvertiserPlacement() !!}"
       data-default-points="{!! ($row < BoardSetting::getMaxRowsPublisher()) ? (BoardSetting::showDefaultPointsPerPublisher($row)) : 1 !!}"
       style="font-size: 0; height: 0;"
    >{!! Board::advertiserPoints(($row < BoardSetting::getMaxRowsPublisher()) ? (BoardSetting::showDefaultPointsPerPublisher($row)) : 1) !!}</a>
    <a href="#"
       class="advertiser_name{!! Board::longString() !!}" id="advertiser_name"
       data-original-title="Enter Advertiser"
       data-type="text"
       data-pk="{!! Board::getAdvertiserPlacement() !!}"
    >{!! Board::advertiserName('Advertiser') !!}</a>
    <div class="minicolors_wrap">
      <!-- <input type="text" value="{!! Board::advertiserColor() !!}" name="color-select" class="pick-a-color form-control hidden"> -->
        <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left">
            <input type="hidden"
                   value="{!! Board::advertiserColor() !!}"
                   data-defaultvalue="{!! Board::advertiserColor() !!}"
                   class="tile_color minicolors-input" id="hidden-input"
                   size="7"
            >
        </div>
    </div>
</div>
