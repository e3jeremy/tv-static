<form role="form" method="post" class="publisher_form">
{!! csrf_field() !!}

    <div class="form-bodyx">
        <div class="row pushtop">
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                    <input type="text"
                           name="publisher_name" id="publisher_name"
                           class="form-control"
                           placeholder="Enter publisher Name"
                           required
                     >
                    <span class="help-block">Some help goes here...</span>
                </div>
            </div>
        </div>
        <div class="row pushtop">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input" style="padding-left: 14px;">
                    <div class="circle" id="call_logo_manager" style="cursor: pointer">
                        <div class="inner_circle"  
						     data-toggle="modal" 
							 data-target="#logo_manager" 
							 style="cursor: pointer"
						>
                          <img id="logo_display" src="" style="max-height: 200px;max-width: 200px; display: none;"/>
                        </div>
                    </div>
                    <input type="text"
                           name="publisher_logo" id="publisher_logo"
                           class="form-control" 
						   placeholder="Add Logo"
                           style="font-size: 0;padding: 0;border: 0;background-color: transparent;margin-top: -57px;margin-left: 42%"
                           required
                     >
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    <div class="form-footer">
        <input type="hidden" name="tile_placement" id="tile_placement" class="form-control" placeholder="Tile Placement #" required>
        <button type="submit" id="submit_btn" class="btn btn-primary form_trigger" onclick="PUBLISHER.submission()">Save</button>
    </div>
</form>
