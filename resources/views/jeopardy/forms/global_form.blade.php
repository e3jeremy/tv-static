<form role="form" method="post" class="global_form" id="global_form">
{!! csrf_field() !!}

    <div class="form-bodyx">
        <div class="form-group form-md-line-input">
            <div class="row pushtop">
                <label for="form_control_1" class="col-md-8 control-label">Column Per Slide</label>
                <div class="col-md-4">
                    <input type="number"
                           name="column_per_slide" id="column_per_slide"
                           class="form-control"
                           value="{!! BoardSetting::getColumnPerSlide() !!}"
                           min="2"
                           max="6"
                           required
                     >
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <div class="row pushtop">
                <label for="form_control_1" class="col-md-8 control-label">Min. Rows Per Publisher</label>
                <div class="col-md-4">
                    <input type="number"
                           name="row_per_publisher" id="row_per_publisher"
                           class="form-control"
                           value="{!! BoardSetting::getRowPerPublisher() !!}"
                           min="4"
                           required
                     >
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <div class="row pushtop">
                <label for="form_control_1" class="col-md-8 control-label">Num. Visible Advertisers</label>
                <div class="col-md-4">
                    <input type="number"
                           name="advertiser_visible" id="advertiser_visible"
                           class="form-control"
                           value="{!! BoardSetting::getAdvertiserVisible() !!}"
                           min="2"
                           required
                     >
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <div class="row pushtop">

                <label for="form_control_1" class="col-md-8 control-label">Max. Rows Per Publisher</label>
                <div class="col-md-4">
                    <input type="number"
                           name="publisher_max_value" id="publisher_max_value"
                           class="form-control"
                           value="{!! BoardSetting::getMaxRowsPublisher() !!}"
                           min="2">
                    <span class="help-block" style="bottom: -3px; position: absolute; text-align: left; right: 11px; width: 285%;">This value will also be the 1st row default points.</span>
                </div>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <div class="row pushtop">
                <label for="form_control_1" class="col-md-8 control-label">Default Initial Goal</label>
                <div class="col-md-4">
                    <input type="number"
                           name="default_goal" id="default_goal"
                           class="form-control"
                           value="{!! BoardSetting::getDefaultGoal() !!}"
                           min="100"
                           required
                     >
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div>
            <p id="" style="text-align: right; font-size: 17px; margin-top: 0px; color: #fff; text-shadow: 1px 2px 2px rgb(0, 0, 0); cursor: pointer"  data-toggle="modal" data-target="#advance-global">Advance Settings</p>
        </div>
    </div>
    <div class="form-footer">
      <button type="submit" id="submit_btn" class="btn btn-primary global_form_trigger">Save</button>
    </div>
</form>
