<div class="filemanager">
    <div class="panel panel-primary">
        <div class="panel-heading ">
            <div class="search">
              <input type="search" id="search_logo" placeholder="Find a file.." />
            </div>

            <div class="breadcrumbs">&nbsp;</div>
        </div>
        <div class="panel-body" style="min-height: 218px;">
          <div class="row">
              <form action="" class="test-dropzone"  id="logo-manager-dropzone">
                    <div class="fallback">
                        <input id='files' name='files' type="file" multiple/>
                    </div>
                    <div class="data" style="padding: 0px 5px;"></div>
              </form>
          </div>
        </div>
    </div>
</div>
