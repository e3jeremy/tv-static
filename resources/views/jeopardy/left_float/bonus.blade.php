
<div id="alert_container"></div>
<div id="bonus_wrap" class="col-md-12">
    <div id="bonus_list" class="panel-group accordion">
        <?php $cnt = CardsAndEvents::countCardsEvents(); ?>
        @foreach(array_reverse(CardsAndEvents::getCardsEvents()) as $index => $event)
        <div id="panel_{!! $event['id'] !!}" class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title" style="position: relative;">
                    <span href="#bonus_list_{!! $cnt !!}"
                       class="accordion-toggle accordion-toggle-styled collapsed"
                       data-parent="#accordion3"
                       data-toggle="collapse"
                       aria-expanded="false"
                       style="color: #fff; cursor: pointer;">
                       BONUS #{!! $cnt !!} :
                    </span>
                    <a href="#"
                        class="outer_bunos_title editable editable-pre-wrapped editable-click" id="outer_bunos_title"
                        data-original-title="Title"
                        data-type="text"
                        data-pk="{!! $event['id'] !!}"
                        style="color:#fff; border-bottom: none;">{!! $event['card_title'] !!}</a>
                </h4>
            </div>
            <div id="bonus_list_{!! $cnt !!}"
                 class="panel-collapse collapse"
                 aria-expanded="false"
                 style="height: 0px;"
            >
                <div class="panel-body">
                    <p>
                        <a href="#"
                            class="outer_bunos_description editable editable-pre-wrapped editable-click" id="outer_bunos_description"
                            data-original-title="Description"
                            data-type="textarea"
                            data-pk="{!! $event['id'] !!}"
                            style="color:#fff; border-bottom: none;">{!! $event['card_description'] !!}</a>
                    </p>
                </div>
            </div>
        </div>
        <?php $cnt--; ?>
        @endforeach
    </div>
</div>
