<div id="global_action">
    <a class="btn btn-bg btn-icon-only toggle_meter" href="javascript:;">
        <i id="hide_meter" class="icon-control-play"></i>
    </a>
    <a class="btn btn-bg btn-icon-only" href="javascript:;">
        <i id="get_global" class="icon-settings" data-toggle="modal" data-target="#global"></i>
    </a>
    <a class="btn btn-bg btn-icon-only" href="javascript:;">
        <i  id="get_cards" class="icon-credit-card" data-toggle="modal" data-target="#cards_table"></i>
    </a>
    <a class="hide-in-frame btn btn-bg btn-icon-only" href="javascript:;">
        <i id="card_event_list" class="icon-list" data-toggle="modal" data-target="#card_event"></i>
    </a>
    <a class="btn btn-bg btn-icon-only" href="javascript:;">
       <i id="refresh_page" class="icon-reload"></i>
    </a>
    <a class="btn btn-bg btn-icon-only" href="javascript:;">
       <i id="add_column" class="icon-plus"></i>
    </a>
  <div style="clear: both"></div>
</div>
