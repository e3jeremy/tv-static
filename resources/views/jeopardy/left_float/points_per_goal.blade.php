<div id="points_goal_wrap" class="col-md-12">
    <h3>
        <span id="points_wrap">{!! PointsMeter::getPoints() !!}</span>
        <span id="divider_wrap"> / </span>
        <span id="goal_wrap">
            <a id="goal"
               class="goal"
               data-original-title="0"
               data-type="text"
               data-pk="test"
             >
                {!! PointsMeter::getGoal() !!}
            </a>
        </span>
    </h3>
</div>
