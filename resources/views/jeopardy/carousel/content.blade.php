<style type="text/css">
    .hide {
        display:none;
    }
    .show{
        display:block;
    }
</style>
<div id="jeopardy-rotator"
    class="carousel slide"
    data-ride="carousel"
    data-interval="false"
>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php
        Board::setPublisherPlacement(1);
        ?>
        @for($item = 1; $item <= Board::numberOfSlides(); $item++)
        <div class="item{!! ($item == 1) ? ' active' : '' !!}">

            <div class="row">
                <div class="col-md-10 jeopardy_board" style="max-height: {!! Board::maxHeight() !!}px">
                    <div id="slide_{!! $item !!}" class="pure-g">
                        <div id="bottom_backdrop"></div>
                        @for($col = 1; $col <=Board::columnPerSlide(); $col++)
                            @if(Board::getPublisherPlacement() <= Board::lastPublisherTilePlacement())
                            <div class="pure-u-1-{!! Board::columnPerSlide() !!} slide_col">
                                <div class="publisher_tile"
                                    data-publisher-id="{!! Board::publisherId() !!}"
                                    data-publisher-tile-placement="{!! Board::getPublisherPlacement() !!}"
                                    style="z-index: 10; position: relative;"
                                >
                                    <div class="publisher-wrap">
                                        <div class="xpublisher" style="position: absolute;font-size: 15px;color: #00b33c;z-index: 99;right: 10px;cursor: pointer;border-radius: 50% !important; display:none"><i class="fa fa-trash" aria-hidden="true"></i></div>
                                        @if(Board::hasPublisherLogo())
                                        <img class="popovers"
                                            src="/{!! Board::publisherLogo() !!}"
                                            border="0"
                                            data-original-title="{!! Board::publisherName() !!}"
                                            data-placement="bottom"
                                            data-trigger="hover"
                                        />
                                        @else
                                        <h2 class="add_publisher">Publisher <br /> Logo</h2>
                                        @endif
                                    </div>
                                </div>
                                @include('jeopardy.board.advertiser_column')
                                <?php Board:: setPublisherMinrow(); ?>
                                @include('jeopardy.board.advertiser_action')
                                <?php Board::incrementPublisherPlacement(); ?>
                            </div>
                            @endif
                        @endfor
                    </div>
                </div>
            </div>
        </div>
        @endfor
    </div>
</div>