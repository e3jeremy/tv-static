@extends('layouts.jeopardy')

@push('style-head')
<link href="/assets/global/plugins/jquery-minicolors/jquery.minicolors.min.css" rel="stylesheet" type="text/css" />

<!-- <link href="/css/jeopardy/grids.css" rel="stylesheet" type="text/css" />
<link href="/css/jeopardy/slideshow.css" rel="stylesheet" type="text/css" />
<link href="/css/jeopardy/style.css" rel="stylesheet" type="text/css" /> -->
<link href="{{ mix("css/app.css") }}" rel="stylesheet"/>
<style>
	.margin-0 {
		margin: 0 !important;
	}
	.padding-0 {
		margin: 0 !important;
	}
	span.minicolors-swatch.minicolors-sprite.minicolors-input-swatch {
		height: 52px;
	}
	.name_wrap .minicolors-theme-bootstrap .minicolors-swatch {
        margin-bottom: 4px;
	}
	.minicolors-position-left .minicolors-panel {
	    left: -1px;
	}
	#global, #cards_table, #card_event, #show_meter{
		z-index: 999999;
	}
	#rotator_wrap{
		width: 100%;
	}
	
	.modal-card_event tbody tr:hover .delete_event {
		display: block;
	}
	.modal-card_event tbody tr .delete_event {
		display: none;
	}
	.modal-card_event tbody td a,
	.modal-card_event tbody td a:hover {
		text-decoration: none;
		border-bottom: none;
		color: #000;
	}
	
	#alert_container .custom-alerts {
		border-radius: 12px !important;
    	border: 4px solid #fff;
	}
	#alert_container .custom-alerts.alert-warning {
    	background-color: red;
	}
	
</style>
@endpush

@section('content')
@include('jeopardy.addins.marqueetext')

<div id="toggle_wrap">
    <a class="toggle_meter" id="show_meter">
        <i class="fa fa-plus-square-o"></i>
    </a>
</div>
<div id="points_meter_wrap">
    @include('jeopardy.left_float.action')
    @include('jeopardy.left_float.points_per_goal')
    @include('jeopardy.left_float.points_meter')
    @include('jeopardy.left_float.bonus')
</div>

<div id="rotator_wrap" class="col-md-8">
    @include('jeopardy.carousel.content')
    @include('jeopardy.carousel.navigation')
</div>
@endsection

@push('script-footer')
<!-- Deffered style to not blocking the rendering of page -->
<noscript id="deferred-styles">


	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">

    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

	<link href="/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/sweetalert.min.css" rel="stylesheet" type="text/css" />
</noscript>
<script>
	var loadDeferredStyles = function() {
		var addStylesNode = document.getElementById("deferred-styles");
		var replacement = document.createElement("div");
		replacement.innerHTML = addStylesNode.textContent;
		document.body.appendChild(replacement)
		addStylesNode.parentElement.removeChild(addStylesNode);
	};
	var raf = requestAnimationFrame || mozRequestAnimationFrame ||
	  webkitRequestAnimationFrame || msRequestAnimationFrame;
	if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
	else window.addEventListener('load', loadDeferredStyles);
</script>

<!-- Load first this modal before the script for it will not function correctly if after the script. -->
@include('jeopardy.modals.logo_manager')
<script src="/assets/js/sweetalert.min.js" type="text/javascript"></script>
<script src="/js/spectrum.min.js" type="text/javascript" type="text/javascript"></script>

<script src="/assets/global/scripts/datatable.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/googleapis/js/jqueryui/1.11.3/jquery-ui.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
<script src="/assets/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>

<script src="/js/jeopardy/lib/cycle2.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/lib/pulsate.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/lib/carousel.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/lib/jquery.flip.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/logo.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/roles.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/bonus.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/common.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/slides.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/global.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/publisher.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/advertiser.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/tile_color.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/points_meter.min.js" type="text/javascript"></script>
<script src="/js/tinycolor-0.9.15.min.js"></script>
<script src="/js/pick-a-color-1.2.3.min.js"></script>
<script src="/js/duterte_color.min.js"></script>
<script src="/js/jeopardy/delete_advertiser.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/stopCarousel.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/delete_publisher.min.js" type="text/javascript"></script>
<script src="/js/jeopardy/draw_card.min.js" type="text/javascript"></script>
<script>
	/**
	 * When jeopardy is the active item in carousel, this function was called by parent frame.
	 */
	var initCycle2 = function () {
	    $('.slideshow').cycle('destroy');
		$('.slideshow').cycle();
		COMMON.pulsateClick2DrawBtn({!! PointsMeter::pointsExceedGoal() !!});
	}
	/**
	 * When parent is done processing the login, it will call this function to pass user roles data.
	 */
	var jSetRoles = function(roles) {
		ROLES.setRoles(roles);
		TILECOLOR.init('', true);
	}

	$(function() {
		ROLES.setRoles({!! json_encode($roles) !!});
		COMMON.animateMeter({!! PointsMeter::getPercent() !!});
		COMMON.pulsateCardIcon({!! CardsAndEvents::countCards() !!});
		COMMON.pulsateClick2DrawBtn({!! PointsMeter::pointsExceedGoal() !!});
		GLOBAL.setUseCustomOffer({!! (BoardSetting::useCustomOffer()) ? 'true' : 'false' !!});
		BONUS.init();
		BONUS.setCardCount({!! CardsAndEvents::countCards() !!});
		PUBLISHER.init({!! json_encode(Board::getPublishers()) !!});
		@if(count($roles) > 0)
		TILECOLOR.init('', true);
		@endif
		TILECOLOR.setColors({!! json_encode(Color::getColors()) !!});
		SLIDES.numberOfSlides({!! Board::numberOfSlides() !!});
		SLIDES.columnPerSlides({!! Board::columnPerSlide() !!});
		SLIDES.publisherCount({!! (Board::getPublisherPlacement() - 1) !!});
		SLIDES.minRows({!! json_encode(Board::minRows()) !!});
		SLIDES.visibleRows({!! json_encode(Board::visibleRows()) !!});
		SLIDES.publishersLastAdvertiserPlacement({!! json_encode(Board::getPublishersLastAdvertiserPlacement()) !!});
	});
</script>

@include('jeopardy.modals.publisher_placement_form')
@include('jeopardy.modals.card_event_table')
@include('jeopardy.modals.global_form')
@include('jeopardy.modals.card_table')
@endpush
