<div class="modal fade bd-example-modal-lg" id="card_event" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-card_event" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h3 class="modal-title" id="myModalLabel">Bunos Events</h3>
          </div>
          <div class="modal-body">
              <div class="row table_wrap">
                <div class="col-md-12">
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                            </div>
                        </div>
                        <table class="table table-striped table-hover table-bordered dataTable no-footer" id="card_event" role="grid" aria-describedby="sample_editable_1_info" style="width: 100%;">

                            <thead>
                              <tr>
                                  <th style="color: #00;">ID</th>
                                  <th style="color: #00;">Points</th>
                                  <th style="color: #00;">Goal</th>
                                  <th style="color: #00;">bonus&nbsp;Flipped</th>
                                  <th style="color: #00;">bonus&nbsp;ID</th>
                                  <th width="192" style="color: #00;">bonus&nbsp;Title</th>
                                  <th style="color: #00;">bonus&nbsp;Description</th>
                                  <th style="color: #00;">instance</th>
                                  <th style="width: 30px; color: #00;">&nbsp;</th>
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
  </div>
</div>
