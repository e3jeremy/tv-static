<div class="modal fade bd-example-modal-lg" id="global" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-global" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="title_action">Global Settings</h3>
            </div>
            <div class="modal-body">
                <div class="portlet-body form">
                    @include('jeopardy.forms.global_form')
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="advance-global" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-global" role="document" style="margin: 8.5% 0 0 30%; width: 600px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="title_action">Advance Settings</h3>
            </div>
            <div class="modal-body">
                <div class="portlet-body form">
                    @include('jeopardy.forms.global_advance_form')
                </div>
            </div>
        </div>
    </div>
</div>
