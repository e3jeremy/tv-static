<div class="modal fade bd-example-modal-lg" id="form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-monopoly" role="document" style="float: left;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="modal-title" id="title_action">Publisher Placement</h3>
      </div>
      <div class="modal-body">
          <div class="portlet-body form">
          @include('jeopardy.forms.publisher_placement_form')
          </div>
      </div>
    </div>
  </div>
</div>
