<div class="modal fade bd-example-modal-lg" id="cards_table" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-card" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="modal-title" id="myModalLabel">List of Bonuses</h3>
      </div>
      <div class="modal-body">
          <div class="row table_wrap">
            <div class="col-md-12">
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <button id="sample_editable_1_new" class="btn green"> Add New
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-bordered dataTable no-footer" id="sample_editable_1" role="grid" aria-describedby="sample_editable_1_info" style="width: 100%;">

                        <thead>
                          <tr>
                              <th>ID</th>
                              <th>Title</th>
                              <th>Description</th>
                              <th>Is&nbsp;Flipped</th>
                              <th style="text-align: right; border-right: 0px none;"><span style="margin-right: -42px;">Action</span></th>
                              <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach( CardsAndEvents::getCards() as $index => $card)
                            <tr>
                              <td>{!! $card['id'] !!}</td>
                              <td>{!! $card['title'] !!}</td>
                              <td >{!! $card['description'] !!}</td>
                              <td id="card_{!! $card['id'] !!}" class="is_flipped_col">{!! $card['is_flipped'] !!}</td>
                              <td><a class="edit" href="javascript:;"> Edit </a></td>
                              <td><a class="delete" href="javascript:;"> Delete </a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
