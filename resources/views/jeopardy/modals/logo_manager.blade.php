<div class="modal fade bd-example-modal-lg" id="logo_manager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" z-index: 99999;">
    <div class="modal-dialog" role="document" style="width: 87%; margin: 3% auto 0">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="myModalLabel">Logo Manager</h3>
            </div>
            <div class="modal-body">
                @include('jeopardy.forms.upload')
                @include('jeopardy.forms.logo_manager')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
