@extends('layouts/master')

@section('content')

    @javascript(compact('pusherKey', 'pusherCluster', 'usingNodeServer'))
    <div class="dashboard" id="dashboard">
        <feedback grid="a2:b3"></feedback>
        <admin-file file-name="Admin" grid="a1:b1"></admin-file>
        <jeopardy grid="c1:e3"></jeopardy>
        <!-- <internet-connection></internet-connection> -->
    </div>
@endsection
