/*!
 * TvProject - EngageIq
 * Publisher object
 * All action, event related to publisher were handled here.

 @class PUBLISHER
 @use X-editable
 **/
var PUBLISHER = (function($) {
	/*
	 * Consist of all publisher details.
	 *
	 * @variable Object
	 */
    var publishers;

	/*
	 * Flag to check if fields is required.
	 *
	 * @variable Bolean
	 */
    var required = false;

	/*
	 *
	 * @variable String
	 */
    var isFormSubmission, formInput, token;
    var title, name;

	/*
	 * Document ready, event listener.
	 *
	 */
    $(function(){
		/*
		 * Add/Edit pulisher.
		 * The placement is not yet occupied.
		 *
		 */
        $('.carousel, .publisher_tile').on('click', '.add_publisher, .publisher-wrap img',
            function() {
                //if(!IsLoggedIn(['admin', 'publisher', 'super_user'])) return;
                //stop the carousel
                var carousel = Object.create(Carousel);
                carousel.stop();
                var pub_id = getPublisherId($(this));
                var tile_placement = getParentTilePlacement($(this));
                var publisher = publishers[tile_placement];

                if(pub_id) {
                    title = 'Edit Publisher';
                    name = 'edit_publisher';
                } else {
                    title = 'Add Publisher';
                    name = 'add_publisher';
                }

                // Empty the fields
                COMMON.clearFields(formInput);
                // Set the correct form attributes and tile placement
                COMMON.setFormAttributes($('#form form.publisher_form'), tile_placement, name, title);

                fillFormFields(publisher);
                $('#form').modal();
           }
       );

		/*
		 * When form is submit.
		 *
		 */
        $('.publisher_form').submit(
            function (event) {
                event.preventDefault();
                if(!isFormSubmission) return;

                var id = getFormId();
                var values = COMMON.getValues(id);

                if(id == 'add_publisher_form') var action = 'add';
                else  var action = 'update';

                processSubmission(values, action, true);
            }
        );

		/*
		 * Action when modal is hidden.
		 *
		 */
        $('#form').on('hidden.bs.modal', function () {
            COMMON.clearFields( $('.publisher_form input'));
            $('#adv_settings_wrap, #logo_manager_wrap').hide();
        });
    });

	/*
	 * Show pop up login modal if not login.
	 * If not inside iframe, login modal is not rquired.
	 *
	 * @param Array/Object roles
	 */
	var IsLoggedIn = function (roles) {
		// If inside iframe
		if( self != top) {
			// Check if its already login, if not a login modal will show.
			if(!parent.parent.modal()){
				return false;
			}
			// Already login
			else {
				// Check user has the ability to edit.
				if(!ROLES.hasAbility2Edit(roles)) {
					COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
					return false;
				}
			}
		}
		// If the page is not iframe.
		else {
			// Check user has the ability to edit.
			if(!ROLES.hasAbility2Edit(roles)) {
				COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
				return false;
			}
		}
		return true;
	}

	/*
	 * Set the primary data of a form before other deatails is set.
	 *
	 */
    var setPrimaryVariables = function() {
        isFormSubmission = false;
        formInput = $('.publisher_form input');
        token = $('.publisher_form input[name=_token]').val();

    };

	/*
	 * Get the publisher id.
	 *
	 * @param Object obj
	 */
    var getPublisherId = function(obj) {
        return $(obj).parents('.publisher_tile').attr('data-publisher-id');
    };

	/*
	 * Get the tile placement
	 *
	 * @param Object obj
	 */
    var getParentTilePlacement = function(obj) {
        return $(obj).parents('.publisher_tile').attr('data-publisher-tile-placement');
    };

	/*
	 * Get the form id
	 *
	 */
	var getFormId = function() {
        return $('.publisher_form').attr('id');
    };

	/*
	 * Process the form submission
	 *
	 * @param Object obj
	 * @param String action
	 * @param Bolean csrf_setup
	 */
    var processSubmission = function (input, action, csrf_setup) {
        if(!csrf_setup) COMMON.ajaxCsrfSetup();

        $.ajax({
            data: input,
            type: 'post',
            url: '/jeopardy/publisher/' + action,
            success: function (result) {
                if (!result.errors) {
                    addResult(action, result, input);
                    updateResult(action, result, input);
                    isFormSubmission = false;
                }
                showDeleteButton();
                //call the delete publisher method
                Publisher.delete();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };
    /**
     * show hide delete button
     */
    var showDeleteButton = function() {
        $(".publisher-wrap")
        .mouseover(function(){
           $(this).find('div.xpublisher').show();
        })
        .mouseout(function(){
           $(this).find('div.xpublisher').hide();
        })
        $(".xpublisher").hover(function(){
            $(this).css('cursor','pointer').attr('title','click to delete this Publisher!');
        }, function() {
            $(this).css('cursor','auto');
        });
    };
	/*
	 * Process the form submission
	 *
	 * @param Object obj
	 * @param String action
	 * @param Bolean csrf_setup
	 */
    var addResult = function (action, result, input) {
        if(action == 'add') {
            // Insert the logo wrapper
            addContent2Tile(result, input);
            // Animate meter
            COMMON.animateMeter(result.percent);

            publishers[result.publisher.tile_placement] = result.publisher;

            COMMON.message('You have successfully added a new publisher.!', title);
            COMMON.hideModal('#form');
            COMMON.clearFields(  $('.publisher_form input'));
        }
    };

	/*
	 * Add the details needed to tile
	 *
	 * @param Object result
	 * @param Object input
	 */
    var addContent2Tile = function (result, input) {
        var tile = $('div[data-publisher-tile-placement=' + input.tile_placement + ']');
        tile.attr('data-publisher-id', result.publisher.id);

        tile.parents('.slide_col')
            .find('.add_adv').attr('data-pub-id', result.publisher.id).end()
            .find('.tile').each(function() {
               $(this).attr('data-publisher-id', result.publisher.id);
        });

        var wrapper = tile.find('.publisher-wrap');
        wrapper.find('h2').hide()
            .end().html(['<div class="xpublisher" style="position: absolute; font-size: 20px; color: rgb(0, 179, 60); z-index: 99; right: 10px; cursor: auto; border-radius: 50% !important; display: none;" title="click to delete this Publisher!"><i class="fa fa-trash" aria-hidden="true"></i></div>','<img class="popovers" src="/' + input.publisher_logo + '" border="0" data-original-title="' + input.publisher_name + '" data-placement="bottom" data-trigger="hover"/>']);
        $('.popovers').popover();
    };

	/*
	 * Add the details needed to tile after updating
	 *
	 * @param String action
	 * @param Object result
	 * @param Object input
	 */
    var updateResult = function (action, result, input) {
        if(action == 'update') {
            // Update logo
            addContent2Tile(result, input);
            // Animate meter
            COMMON.animateMeter(result.percent);
            publishers[result.publisher.tile_placement] = result.publisher;

            COMMON.message('You have successfully updated the publisher.!', title);
            COMMON.hideModal('#form');
            COMMON.clearFields(  $('.publisher_form input'));

        }
    };

	/*
	 * Add the logo to tile after updating
	 *
	 * @param Object result
	 * @param Object input
	 */
    var updatelogo = function (result, input) {
        $('div[data-publisherid = ' + result.publisher_id + ']').find('img').attr('src', '/' + input.publisher_logo);
    };

	/*
	 * Fill the form fields when showing the form modal
	 *
	 * @param Object result
	 */
    var fillFormFields = function (result) {
        $.each(result, function( key, value ) {
            $('#' + key).val(value);
            if(key == 'publisher_logo' && value) {
                $('#' + key).show();
                if(ImageExists(window.location.protocol + "//" + window.location.host + "/" + value)){
                    $('#logo_display').show().attr('src', '/' + value).show();
                }
            }
        });
    };

    /*
     * Check if logo exists
     *
     * @param String url
     */
    function ImageExists(url)
    {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, true);
        http.send();
        return http.status != 404;
    }

	/*
	 * Set the publisher variable with data
	 *
	 * @param Object data
	 */
    var setPublishers = function(data) {
        var $self = this;
        publishers = data;
    };

	/*
	 * Public Dmethod
	 *
	 */
    var publicMethod = {
        init: function(data) {
            // set needed variables
            setPrimaryVariables();
            setPublishers(data);
        },
        submission: function() {
            isFormSubmission = true;
        }
    }

    return publicMethod;
})(jQuery);