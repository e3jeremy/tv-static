var Card = function() {
	var _draw = function() {
		setInterval(function() {
			$.ajax({
				url: '/jeopardy/points-meter/get-card-reached-goal',
				type: 'get',
				success: function(msg){
					if($.isNumeric(msg)){
						$.ajax({
							url:'/jeopardy/card/get-random-card',
							type:'post',
							success: function(){
								swal({
								  	title: "Card Drawn",
								  	text: "Card was drawn, This will close in 10 seconds.",
								  	imageUrl: "tvlogo/thumbs-up.jpg",
								  	timer: 20000,
								  	showConfirmButton: false
								});
								setTimeout(function() {
									console.log("refresh triggered after card drawn");
									$("i#refresh_page").trigger("click");
								}, 25000);
							},
							error: function(error) {
								console.log(error);
							}
						})
					}else{
						$("i#refresh_page").trigger("click");
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}, 3600000);//one hour
	}
	return {
		draw: function() {
			_draw();
		}
	}
}();
jQuery(document).ready(function(){
	Card.draw();
});
// 3600000