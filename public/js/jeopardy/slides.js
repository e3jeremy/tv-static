/*!
 * TvProject - EngageIq
 * Slides object
 * All action, event related to slides were handled here.

 @class SLIDES
 @use carousel
 @use cycle2
 **/
var SLIDES = (function($) {
	/*
	 * Set the autoSelector for cycle2
	 *
	 */
    $.fn.cycle.defaults.autoSelector = '.slideshow';

	/*
	 * Number of slides
	 *
	 * @variable Integer
	 */
    var number_of_slides;

	/*
	 * Minimum number of rows per column
	 *
	 * @variable Integer
	 */
    var min_rows;

	/*
	 * Set publishers last advertiser placement
	 *
	 * @variable Integer
	 */
    var publisher_rows = {};

	/*
	 * Number of visible rows per column
	 *
	 * @variable Integer
	 */
    var visible_rows;

	/*
	 * Publisher count
	 *
	 * @variable Integer
	 */
    var publishers_counts;

	/*
	 * Number of column per slides
	 *
	 * @variable Integer
	 */
    var column_per_slides;

	/*
	 * Set number of slides
	 *
	 * @param Interger $num_of_slides
	 */
    var setNumberOfSlides = function($num_of_slides) {
        number_of_slides = $num_of_slides;
    };

	/*
	 * Set number of minimum of rows per column
	 *
	 * @param Interger $min_rows
	 */
    var setMinRows = function($min_rows) {
        min_rows = $min_rows;
    };

	/*
	 * Set number of visible rows per column
	 *
	 * @param Interger $visible_rows
	 */
    var setVisibleRows = function($visible_rows) {
        visible_rows = $visible_rows;
    };

	/*
	 * Set publisher count
	 *
	 * @param Interger $pubs_count
	 */
    var setPublisherCounts = function($pubs_count) {
        publishers_counts = $pubs_count;
    };

	/*
	 * Set column per slides
	 *
	 * @param Interger $column_per_slides
	 */
    var setColumnPerSlides = function($column_per_slides) {
        column_per_slides = $column_per_slides;
    };

	/*
	 * Set publishers last advertiser placement
	 *
	 * @param Interger $publisher_rows
	 */
    var setpublishersLastAdvertiserPlacement = function($publisher_rows) {
        publisher_rows = $publisher_rows;
    };

	/*
	 * Document ready, event listener.
	 *
	 */
    $(function(){
		/*
		 * Series of action when carousel is sliding.
		 *
		 */
		$('#jeopardy-rotator').on('slide.bs.carousel', function () {
      //stop the carousel
      var carousel = Object.create(Carousel);
      carousel.stop();
			$('.carousel-control').hide();
			$('.slideshow').cycle('destroy');
			setTimeout(function() {
				$('.active .slideshow').cycle();
				$('.carousel-control').show();
			}, 800);
		});

		/*
		 * Add advertiser/row on click.
		 *
		 */
		$('.carousel').on('click', '.add_adv', function () {
      //stop the carousel
      var carousel = Object.create(Carousel);
      carousel.stop();
      var pub_id = $(this).data('pub-id');
      var col = $(this).data('col');

      if( self != top) {
        if(!parent.parent.modal()) return;
      }

      if(!pub_id) {
        toastr.info('Please add publisher first.', 'Action not Available');
        return;
      }

      if((publisher_rows['pub' + col] + 1) > $('#publisher_max_value').val()) {
        swal({
          title: "You are about to exceed the maximum advertisers per publisher!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Proceed anyway!"
        },
        function(isConfirm) {
            if(isConfirm) prependAdvertiser(pub_id, col);
        });
      } else {
        prependAdvertiser(pub_id, col);
      }
		});

    var prependAdvertiser = function (pub_id, col) {
    var prepend = '';
    prepend += '<div class="advertiser tile more"\
                     data-publisher-id="' + pub_id + '"\
                     data-tile-placement="' + (publisher_rows['pub' + col] + 1) + '"\
                 >\
                    <div class="advertiser-wrap">\
                      <div class="x" style="position: absolute;font-size: 30px;color: #00b33c;z-index: 99;right: 10px;cursor: pointer;border-radius: 50% !important; display:none"><i class="fa fa-trash" aria-hidden="true"></i></div>';
    prepend +=          advertiserLogo((publisher_rows['pub' + col] + 1));
    prepend +=          advertiserName((publisher_rows['pub' + col] + 1));
    prepend += '        <div class="stats_wrap">';
    prepend +=              advertiserPrice((publisher_rows['pub' + col] + 1));
    prepend +=               advertiserVolume((publisher_rows['pub' + col] + 1));
    prepend += '            <div style="clear: both;"></div>\
                       </div>\
                    </div>\
               </div>';

    var $_col = $('div.vertical[data-col=' + col + ']');
    $_col.cycle('add', prepend);
    $_col.cycle('destroy');
    $_col.cycle();
    console.log(publisher_rows);
    var col_row = publisher_rows['pub' + col];
    $_col.cycle('goto', col_row - (visible_rows - 1));
    publisher_rows['pub' + col] = col_row + 1;
    ADVERTISER.init();
    }

		/*
		 * Add slides on click.
		 *
		 */
		$('#add_slides').on('click', function(){
			  if( self != top) {
				  if(!parent.parent.modal()) return;
			  }
			  var publisher_tile_placement = (publishers_counts + 1);
			  var $tile_placement = 1;
			  var append = '';
			  append += '<div class="item">\
							<div class="row" style="margin-top: 44px; xpadding-bottom: 200px;">\
								<div class="col-md-10 jeopardy_board">\
									<div class="pure-g">';
									for(var add_col = 1; add_col <= column_per_slides; add_col++) {
			append +=                   publisherColumn(publisher_tile_placement);
									}
			append += '             </div>\
								</div>\
							</div>\
						</div>';

		  $('.carousel-inner').append(append);
		  $('#jeopardy-rotator').carousel(number_of_slides);
		  $('.slideshow').cycle();

		  publishers_counts = publisher_tile_placement - 1;
		  number_of_slides = number_of_slides + 1;

		  ADVERTISER.init();

		});

		/*
		 * Add column  on click.
		 *
		 */
		$('#add_column').on('click', function(){
			if(publishers_counts % column_per_slides === 0) {
				publishers_counts = publishers_counts + 1
				addslides(publishers_counts);
			} else {
				publishers_counts = publishers_counts + 1;
				$('#slide_' + number_of_slides).append(publisherColumn(publishers_counts));
				$('.slideshow').cycle('destroy');
				$('.slideshow').cycle();
				ADVERTISER.init();
			}

		});
    });

	/*
	 * Add slides.
	 *
	 * @param Integer publisher_tile_placement
	 */
    var addslides = function (publisher_tile_placement) {
          var $tile_placement = 1;
          var slide = number_of_slides + 1;
          var append = '';
          append += '<div class="item">\
                        <div class="row" style="margin-top: 44px; xpadding-bottom: 200px;">\
                            <div class="col-md-10 jeopardy_board">\
                                <div id="slide_' + slide + '" class="pure-g">';
                                for(var add_col = 1; add_col <= 1; add_col++) {
          append +=                   publisherColumn(publisher_tile_placement);
                                  }
          append += '             </div>\
                              </div>\
                          </div>\
                      </div>';

        $('.carousel-inner').append(append);
        $('#jeopardy-rotator').carousel(number_of_slides);
        number_of_slides = slide;
        $('.slideshow').cycle();
        ADVERTISER.init();
    }

	/*
	 * Add column.
	 *
	 * @param Integer publisher_tile_placement
	 */
    var publisherColumn = function (publisher_tile_placement) {
      var html = '';
      html += '       <div class="pure-u-1-' + column_per_slides + ' slide_col">\
                          <div class="publisher_tile"\
                               data-publisher-id=""\
                               data-publisher-tile-placement="' + publisher_tile_placement + '"\
                                style="z-index: 10; position: relative;"\
                           >\
                              <div class="publisher-wrap">\
                                  <h2 class="add_publisher">Add <br /> Publisher</h2>\
                              </div>\
                          </div>\
                          <div class="slideshow vertical"\
                               data-col="' + publisher_tile_placement + '"\
                               data-cycle-slides="> .advertiser"\
                               data-cycle-fx="carousel"\
                               data-cycle-timeout="0"\
                               data-cycle-next="#nextad' + publisher_tile_placement + '"\
                               data-cycle-prev="#prevad' + publisher_tile_placement + '"\
                               data-cycle-carousel-visible="' + visible_rows + '"\
                               data-cycle-carousel-vertical="true"\
                               data-allow-wrap="false"\
                          >';
        html +=               advertisersColumn(publisher_tile_placement);
        html += '         </div>\
                          <div class="vertical_navigation">';
        html +=               verticalNavigation(publisher_tile_placement);
        html += '         </div>';
                          publisher_tile_placement++;
        html += '      </div>';

        return  html;
    }

	/*
	 * Vertical nvigation/cycle2 html.
	 *
	 * @param Integer publisher_tile_placement
	 */
    var verticalNavigation = function (publisher_tile_placement) {
        html = '';
        html += '<a href="#" class="add_adv" id="add_ad_' + publisher_tile_placement + '"\
                      data-col="' + publisher_tile_placement + '"\
                      data-pub-id=""\
                      style="font-size: 20px"\
                  > + </a>\
                  <br />\
                  <a href="#" class="prevad" id="prevad' + publisher_tile_placement + '">\
                        <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>\
                        <span class="sr-only">Previous</span>\
                  </a>\
                  <a href="#" class="nextad" id="nextad' + publisher_tile_placement + '">\
                        <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>\
                        <span class="sr-only">Previous</span>\
                   </a>';

        return  html;
    };

	/*
	 * Column html.
	 *
	 * @param Integer publisher_tile_placement
	 */
    var advertisersColumn = function (publisher_tile_placement) {
        html = '';
        for(var tile_placement = 1; tile_placement <= min_rows; tile_placement++) {
        html += '<div class="advertiser tile"\
                       data-advertiser-id=""\
                       data-publisher-id=""\
                       data-tile-placement="' + tile_placement + '"\
                   >\
                      <div class="advertiser-wrap">';
      html +=             advertiserLogo(tile_placement);
      html +=             advertiserName(tile_placement);
      html += '           <div class="stats_wrap">';
      html +=                 advertiserPrice(tile_placement);
      html +=                 advertiserVolume(tile_placement);
      html += '               <div id="backdrop"></div>\
                              <div style="clear: both;"></div>\
                          </div>\
                      </div>\
                </div>';
        }

        return html;
    };

	/*
	 * Advertiser logo html.
	 *
	 * @param Integer tile_placement
	 */
    var advertiserLogo = function(tile_placement) {
        html = '';
        html += '<div class="logo_wrap">\
                <h3 style="cursor: pointer">Advertiser<br /> logo</h3>\
                <img id="logo_display_pyramid" style="cursor: pointer; display: none;" src="" border="0"/>\
            </div>';
        return html;
    };

	/*
	 * Advertiser name html.
	 *
	 * @param Integer tile_placement
	 */
    var advertiserName = function(tile_placement) {
        html = '';
        html += '<div class="name_wrap" style="background-color: #2e3092">\
                       <a href="#"\
                          class="points" id="points"\
                          data-original-title="Enter Points"\
                          data-type="text"\
                          data-pk="' + tile_placement + '"\
                          data-default-points="' + setPoints(tile_placement) + '"\
                          style="font-size: 0; height: 0;"\
                      >' + parseFloat(setPoints(tile_placement)).toFixed(2) + '</a>\
                      <a href="#"\
                         class="advertiser_name" id="advertiser_name"\
                         data-original-title="Enter Advertiser"\
                         data-type="text"\
                         data-pk="' + tile_placement + '"\
                      >Advertiser</a>\
                      <div class="minicolors_wrap">\
                         <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left">\
                             <input type="hidden"\
                                    value="#2e3092"\
                                     data-defaultValue="#2e3092"\
                                    class="tile_color minicolors-input" id="hidden-input"\
                                    size="7"\
                              >\
                         </div>\
                      </div>\
                  </div>';
        return html;
    };

	/*
	 * Set the necessary points for the advertiser.
	 *
	 * @param Integer tile_placement
	 */
    var setPoints = function (tile_placement) {
        var points = 1.00;
        if(tile_placement <= $('#publisher_max_value').val()) {
            points = (parseInt($('#publisher_max_value').val()) + 1) - tile_placement;
        }
        return points;
    }

	/*
	 * Advertiser price html.
	 *
	 * @param Integer tile_placement
	 */
    var advertiserPrice = function(tile_placement) {
        html = '';
        html += '<div class="price_wrap">\
                     <div class="col-md-6" style="border-right: 1px solid #fff; padding: 0px;">\
                         <a href=""\
                             class="price" id="price"\
                             data-original-title="Enter Price"\
                             data-type="text" data-pk="' + tile_placement + '"\
                          >Price</a>\
                      </div>\
                  </div>';
        return html;
    };

	/*
	 * Advertiser volume html.
	 *
	 * @param Integer tile_placement
	 */
    var advertiserVolume = function(tile_placement) {
        html = '';
        html += '<div class="volume_wrap">\
                      <div class="col-md-6">\
                          <a href="" class="volume" id="volume" \
                             data-original-title="Enter Volume" \
                             data-type="text" data-pk="' + tile_placement + '"\
                           >Volume</a>\
                      </div>\
                  </div>';
        return html;
    };

 	/*
	 * Public method.
	 *
	 */
    var publicMethod = {
        init: function() {
        },
        numberOfSlides: function($num_of_slides) {
            setNumberOfSlides($num_of_slides);
        },
        minRows: function($min_rows) {
            setMinRows($min_rows);
        },
        visibleRows: function($visible_rows) {
            setVisibleRows($visible_rows);
        },
        getVisibleRows: function() {
            return visible_rows;
        },
        columnPerSlides: function($column_per_slides) {
            setColumnPerSlides($column_per_slides);
        },
        publisherCount: function($pubs_count) {
            setPublisherCounts($pubs_count);
        },
        publishersLastAdvertiserPlacement: function($publisher_rows) {
            setpublishersLastAdvertiserPlacement($publisher_rows);
        }
    }
    return publicMethod;
})(jQuery);
