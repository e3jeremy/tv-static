/*!
 * TvProject - EngageIq
 * Tile color object
 * All action, event related to tile color were handled here.

 @class TILECOLOR
 @use Minicolors
 **/
var TILECOLOR = (function($) {

    /*
     * Colors container in relation to advertiser name.
     *
     */
    var colors = {};
    /*
     * Default color for engageiq.
     *
     */
    var engageiq_color = '#00cc00';
     /**
      * @swatches, not brand of relo ok
      * fixed swatches length must be 126
      *
      * @type {[array]}
      */
    var swatches = function () {
        return [
            "#242E35","#041004", "#130A06", "#315BA1", "#080110", "#082567", "#09255D", "#327DA0",
            "#002FA7", "#0033CC", "#0033FF", "#370202", "#3B0910", "#006699", "#FFD1DC", "#FFD700",
            "#0066CC", "#0066FF", "#3E1C14", "#0099CC", "#0099FF", "#FFC0CB", "#FF7D07", "#FD0E35",
            "#480607", "#330000", "#330033", "#330066", "#330099", "#3300CC", "#FF91A4", "#FCC01E",
            "#3300FF", "#333366", "#333399", "#3333CC", "#3333FF", "#FBF9F9", "#FF8C69", "#FFC901",
            "#336699", "#3366CC", "#3366FF", "#3399CC", "#3399FF", "#FFDDF4", "#FFCBA4", "#FBE7B2",
            "#003366", "#0076A3", "#010D1A", "#33FFFF", "#660000", "#660033", "#660066", "#6600CC",
            "#6600FF", "#663300", "#663333", "#663366", "#663399", "#6633CC", "#6633FF", "#FFEFEC",
            "#666666", "#666699", "#6666CC", "#6666FF", "#990000", "#990033", "#990066", "#D9B99B",
            "#990099", "#9900CC", "#9900FF", "#993300", "#993333", "#993366", "#993399", "#9933CC",
            "#9933FF", "#996600", "#996633", "#996666", "#996699", "#9966CC", "#9966FF", "#999999",
            "#CC0000", "#CC0033", "#CC0066", "#CC0099", "#CC00CC", "#CC00FF", "#CC3300", "#CC3333",
            "#CC3366", "#CC3399", "#CC33CC", "#CC33FF", "#CC6600", "#CC6633", "#CC6699", "#CC66CC",
            '#CC9900', "#CC9933", "#4E420C", "#4D3833", "#4C3024", "#FF0000", "#FF0033", "#FFE1DF",
            "#FF0066", "#FF0099", "#FF00FF", "#FF3300", "#FF3333", "#FF3366", "#FF3399", "#FF33CC",
            "#FF6600", "#FF6633", "#FF9900", "#FF9933", "#FFCC00", "#FFCC33","#353542"
        ];
    }

    /*
     * Set options for mini colors.
     *
     */
    var options = {
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: 'top left',
        change: function(hex, opacity) {
            if (!hex) return;
            // Hide swatches
            hideMiniColor();
        },
        theme: 'bootstrap',
        format: 'hex',
        hide: true,
        hideSpeed: 100,
        //swatches: color_swatches,//you can set the value depending of what array of colors you want to display, use COLORS.getWatches()
        swatches: swatches(),
        show: function(e) {
            //stop the carousel
            var carousel = Object.create(Carousel);
            carousel.stop();
            // Pre process on show
            var el = $(this);
            $('.jeopardy_board').css('overflow', 'auto'); // Make the pop up to overflow
            el.parents('.tile').find('.name_wrap > div').css('z-index', 99); // make this div above else
            el.parents('.vertical').css('overflow', 'visible'); // Make the column overflow
            el.parents('.pure-g').find('.publisher_tile').css('z-index', 1); // Make the publisher tile below
            hideExtraAdvertisersWhenOverflowAllowed(el);  // hide because overflow was allowed
        },
        hide: function() {
            var el = $(this);
            var $tile = $(this).parents('.tile');
            // Pre process on hide
            $('.jeopardy_board').css('overflow', 'hidden'); // Removed overflow
            $tile.find('.name_wrap > div').css('z-index', 1); // Make same level as the others
            el.parents('.vertical').css('overflow', 'hidden'); //  // Removed overflow
            el.parents('.pure-g').find('.publisher_tile').css('z-index', 10); // Return to default
            el.parents('.vertical').find('.more').each(function () { // display for it will be used for sliding up/down
                $(this).show();
            });

            // Do nothing if color is not changed.
            var selected_color = el.val();
            if(selected_color == el.parents('.name_wrap').attr('data-color')) return;

            // Alert something if green is selected
            $current_background = $tile.find('input.tile_color').attr('data-defaultvalue');
            green_is_restricted_color($tile, $current_background, selected_color);

            // If parent tile(publisher) is occupied, ignore if not.
            if ($tile.data('publisher-id')) {
                // Get the actual name of color
                var color_name = giveMeTheFuckinColorName(selected_color);

                // Update process
                if(taken_by = is_taken(selected_color)) {
                    // If taken/already used
                    swal(colorExistedConfirmation (color_name, selected_color, taken_by), function(isConfirm) {
                        if (swalColorConfirmed($tile, $current_background, isConfirm)) update_advertiser_color ($tile, selected_color, color_name, 'pk-update');
                    });
                } else {
                    swal( colorConfirmationMessage(color_name, selected_color) , function(isConfirm) {
                        if (swalColorConfirmed($tile, $current_background, isConfirm)) update_advertiser_color ($tile, selected_color, color_name, 'pk-update');
                    });
                }
            }
        }
    };

    // When the color pallete wasshown the board allowed overflow which the extra advertiser blow are shown and should not be.
    // Aworkaround to hide it.
    var hideExtraAdvertisersWhenOverflowAllowed = function (el) {
        var from_active = 0;
        var start_counting = false;
        el.parents('.vertical').find('.cycle-carousel-wrap .advertiser').each(function () { // hide because overflow was allowed
            if($(this).hasClass('cycle-slide-active')) start_counting = true;
            if(start_counting) from_active++;
            // Hide below the visible rows
            if(from_active > SLIDES.getVisibleRows()) $(this).hide();
        });
    }

    // Green is limited to only one advertiser - EngageIq
    var green_is_restricted_color = function ($obj, color, color_val) {
        if (color_val === '#274e13') {
            swal({
                type: 'warning',
                title: 'Oops!',
                text: 'Sorry, that color is only for EngageIq. Please select again.'
            });

            updateColor($obj, color);
        }
    }

    // Determine if color is already used.
    var is_taken = function (selected_color) {
        // Check if color is used by the other adveriser
        var taken = '';
        $.map( colors, function( value, index ) {
            if(selected_color == value){
                taken = index;
            }
        });
        return taken;
    }

    // Update database
    update_advertiser_color = function ($tile, selected_color, color_name, slug) {

        $.ajax({
            data: {
                publisher_id: $tile.data('publisher-id'),
                pk: $tile.data('tile-placement'),
                name: 'color',
                value: selected_color,
                advertisers: $tile.find('#advertiser_name').text().toLowerCase()
            },
            type: 'post',
            url: '/jeopardy/advertiser/' + slug,
            success: function(data) {
                // Update others and show message
                done_updating (data, color_name);
                // Update background color, it was not affected by minicolor update
                $tile.find('.name_wrap').css('background-color', data.advertiser.color)
                                        .attr('data-color', data.advertiser.color);
                // Update @var colors
                TILECOLOR.updateAdvertiserColor(data.advertiser.advertiser_name.toLowerCase(), data.advertiser.color);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    }

    /**
     * Process after update.
     */
    var done_updating = function (data, color_name) {
        swal("Done!", "Color " + color_name + " applied successfuly!", "success");
        updateSameAdvertiser(data);
    }

    /**
     * Update the colors of other advertiser with same name.
     */
    var updateSameAdvertiser = function (data) {

        // Go through all tiles
		$('.advertiser.occupied #advertiser_name').each(function () {
			if($(this).text().toLowerCase() == data.advertiser.advertiser_name.toLowerCase()) {
				$(this).parents('.name_wrap').css('background-color', data.advertiser.color)
				       .find('.tile_color')
                       .minicolors('destroy') // Destroy for easy updates
                       .val(data.advertiser.color)
                       .attr('data-defaultvalue', data.advertiser.color);
                // Reinitialized
                TILECOLOR.init($(this).parents('.tile'), false);
			}
        });
    }

    /**
     * let the user confirm the color that is existing to other advertiser before saving.
     */
    var colorExistedConfirmation = function (color_name, selected_color, taken_by) {
        return {
            title: "Oops! Looks like the selected color <span style=\"color: " + selected_color + "\">" + color_name + "</span><br />is already used by" + ' ' + taken_by,
            text: "Looks like the selected color " + color_name + " is already used by" + ' ' + taken_by,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Proceed anyway!",
            closeOnConfirm: false,
            html: true
        };
    }

    /**
     * let the user confirm the color before saving.
     */
    var colorConfirmationMessage = function (color_name, selected_color) {
        return {
            title: "Are you sure to use this color <span style=\"color: " + selected_color + "\">" + color_name + "</span>?",
            text: "Are you sure to use this color " + color_name + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: true,
            closeOnCancel: true,
            html: true
        };
    }

    /**
     * If the confirmation is cancelled
     */
    var swalColorConfirmed = function ($obj, color, isConfirm) {
        if (!isConfirm) updateColor($obj, color);
        return isConfirm;
    }

    /**
     * Update the color on tile
     */
    var updateColor = function ($tile, color) {
        $tile.find('.name_wrap').css('background-color', color)
                                .attr('data-color', color)
                                .find('.tile_color').minicolors('destroy')
                                                    .attr('data-defaultvalue', color)
                                                    .val(color);
        TILECOLOR.init($tile, false);

    }

    /**
     * [hidePantyColor description]
     * @return {[type]} [description]
     */
    var hidePantyColor = function () {
        $(".tile").on("click", '.minicolors-swatch-color', function() {
            hideMiniColor();
        });
    }

    var hideMiniColor = function () {
        $('.tile_color').minicolors('hide');
    }
    /**
     * show the hex equivalent color name to the user with big boobs
     * @param  {[type]} color [description]
     * @return {[type]}       [description]
     */
    var giveMeTheFuckinColorName = function (color) {
        var n_match = ntc.name(color);
        n_rgb = n_match[0]; // RGB value of closest match
        n_name = n_match[1]; // Text string: Color name
        n_exactmatch = n_match[2]; // True if exact color match

        return n_name;
    }
	/*
	 * Set the html entity.
	 *
	 * @param Element obj
	 */
    var htmlEntity = function (obj) {
        if(obj) return obj.find('.tile_color');
        return $('.tile_color');
    }

    /*
     * Document ready, event listener.
     *
     */
    $(function(){

    });

    /*
     * Public method.
     *
     */
    var publicMethod = {
        init: function(obj, remove) {

            var $tile_color = htmlEntity(obj);
            $tile_color.minicolors(options);

            if (remove) {
                $('div.advertiser').each(function() {
                    if($(this).attr('data-advertiser-id') == '') {
                        $(this).find('.tile_color').minicolors('destroy');
                    }
                    if($(this).find('.advertiser_name').text().toLowerCase() == 'engageiq') {
            			$(this).find('.name_wrap').find('.tile_color').minicolors('destroy');
            		}
                });
            }
        },
        setColors: function($colors) {
            colors = $colors;
        },
        getColors: function() {
            return colors;
        },
        updateAdvertiserColor: function($advertiser, $color) {
            colors[$advertiser] = $color;
        },
        advertiserHasColor: function($advertiser) {
            if ($advertiser in colors) {
                return true;
            }
            return false;
        },
        getColorOfAdvertiser: function($advertiser) {
            return colors[$advertiser];
        }
    }
    return publicMethod;
})(jQuery);
