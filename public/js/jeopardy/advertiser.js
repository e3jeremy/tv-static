/*!
 * TvProject - EngageIq
 * Advertiser object
 * All action, event related to advertiser were handled here.

 @class ADVERTISER
 @use X-editable
 **/
var ADVERTISER = (function($) {
	/*
	 * Set the url for ajax process of x-editable form
	 *
	 */
	$.fn.editable.defaults.url = '/jeopardy/advertiser/pk-update';
	
	/*
	 * Add class on a tile/box/placement when advertiser occupied it.
	 *
	 * @param Elememt tile
	 * @param Object data
	 */
    var occupied = function (tile, data) {
        tile.addClass('occupied').attr('data-advertiser-id', data.advertiser.id);
    };

	/*
	 * Initialize tile of advertiser name section to be able to change color
	 *
	 * @param Elememt tile
	 * @param Bolean _new
	 */
	var initializeTileColor = function (tile, _new) {
        if(_new) TILECOLOR.init(tile, false);
	}

	/*
     * Update Meter when parameters meet.
	 *
	 * @param Object data
	 */
    var updateMeter = function (data) {
        POINTSMETER.updatepointsMeter(data);
    };

	/*
	 * Validation for x-ediable fields
	 *
	 * @param String value
	 * @param Element tile
	 * @param Bolean update_meter
	 * @param Bolean is_number
	 */
    var validate = function (value, tile, update_meter, is_number) {
		// If fields is required.
        if ($.trim(value) == '') return 'Please filled up the fields before submitting.';
		// If Editing fields when no publisher on the same column.
        if(!tile.attr('data-publisher-id')) return 'Add publisher first.';
		// If updating points field.
        if(update_meter) {
            if(parseFloat($('#points_wrap').text()) >= parseFloat($('#goal_wrap a').text())) {
                return 'Please update the goal value to be greater than the points value.';
            }
        }
		// If fields require number
        if(is_number) {
            if (!COMMON.isNumeric(value))   return 'Please use number and decimals only.';
        }
    };

	/*
	 * Action during the showing of x-editable fields.
	 *
	 * @param Object tile
	 * @param Bolean show_backdrop
	 */
    var onShown = function (tile, show_backdrop) {
		// Make the column remove the overflow-hidden.
        tile.addClass('clicked').parents('.vertical').css('overflow', 'visible');
		// Remove backdrop
        if(show_backdrop) tile.find('.stats_wrap').find('#backdrop').addClass('modal-backdrop fade in');
    };

	/*
	 * Action during the hiding of x-editable fields.
	 *
	 * @param Object tile
	 * @param Bolean show_points
	 * @param Bolean hide_backdrop
	 */
    var onHide = function (tile, show_points, hide_backdrop) {
		//Make column disregard overflow.
        tile.removeClass('clicked').parents('.vertical').css('overflow', 'hidden');

		// Action to show points x-editable fields
        if(show_points && tile.hasClass('occupied')) {
			// Check if user is super user in roles
			// The points fields will show if roles is super_user
			if(ROLES.has('super_user') &&
				tile.find('#advertiser_name').text().toLowerCase() == 'engageiq'
			) {
				// Set time out to activate the points x-editable smoothly.
				setTimeout(function() {
					tile.find('#points').editable('show');
				}, 200);
			} else {
				hide_backdrop = true;
			}

        } else {
			hide_backdrop = true;
		}
		// Hide backdrop
        hideBackdrop(tile.find('.stats_wrap').find('#backdrop'), hide_backdrop);
    };


	/*
	 * Hide backdrop of x-editable
	 *
	 * @param Object backdrop
	 * @param Bolean hide_backdrop
	 */
	var hideBackdrop = function (backdrop, hide_backdrop) {
		if(hide_backdrop) backdrop.removeClass('modal-backdrop fade in');
	}

	/*
	 * If the empty value for fields that require number
	 *
	 * @param Object editable
	 */
    var showValue = function (editable) {
        if (!COMMON.isNumeric(editable.input.$input.val())) {
              editable.input.$input.val('00');
        }
    };

	/*
	 * Check if the row have a publisher.
	 *
	 * @param Object obj
	 */
	var checkIfAble2Add = function (obj)
	{
		if(!obj.parents('.tile').attr('data-publisher-id')) {
			COMMON.message('Add publisher first.', 'Unable To Add');
			disableEditable(obj);
		} else {
			//enableEditable(obj);
		}
	}

	/*
	 * Show pop up login modal if not login.
	 * If not inside iframe, login modal is not rquired.
	 *
	 * @param Object obj
	 */
	var checkIfLoggedIn = function (obj, roles) {
		// If inside iframe
		if( self != top) {
			// Check if its already login, if not a login modal will show.
			if(!parent.parent.modal()){
				// Disable x-editable if not logged in.
				disableEditable(obj);
			}
			// Already login
			else {
				// Check user has the ability to edit.
				if(ROLES.hasAbility2Edit(roles)) {
					// Enable x-editable.
					enableEditable(obj);
				} else {
					// Disable x-editable.
					disableEditable(obj);

					// Since the fields for points is hidden and
					// only in advertiser name can be access.
					// Show points even the advertiser name is not editable
					// Unless the user had the ability to edit points
					if(ROLES.has('super_user')) {
						// Set time out to activate the points x-editable smoothly.
						setTimeout(function() {
							obj.parent().find('#points').editable('show');
						}, 200);

						return;
					}
					COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
				}
			}
		}
		// If the page is not iframe.
		else {
			// Check user has the ability to edit.
			if(ROLES.hasAbility2Edit(roles)) {
				// Enable x-editable if logged in.
				enableEditable(obj);
			} else {
				// Disable x-editable if not logged in.
				console.log('disable editable');
				disableEditable(obj);

				// Since the fields for points is hidden and
                // only in advertiser name can be access.
				// Show points even the advertiser name is not editable
				// Unless the user had the ability to edit points
				if(ROLES.has('super_user')) {
					// Set time out to activate the points x-editable smoothly.
					setTimeout(function() {
						obj.parent().find('#points').editable('show');
					}, 200);

					return;
				}

				COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
			}
		}
	}

	/*
	 * Enable x-editable.
	 *
	 * @param Object obj
	 */
	var enableEditable = function (obj) {
		if(obj.hasClass('editable') && obj.hasClass('editable-disabled')) {
			obj.editable('toggleDisabled');
		}
	}

	/*
	 * Disable x-editable.
	 *
	 * @param Object obj
	 */
	var disableEditable = function (obj) {
		if(obj.hasClass('editable') && !obj.hasClass('editable-disabled')) {
			obj.addClass('editable-disabled');
			obj.editable('toggleDisabled');
		}
	}

	/*
	 * Set all anchor in board to x-editable fields.
	 * Fields are 'advertiser name', 'points', 'price', 'volume'
	 *
	 */
    var setEditable = function () {
		// Set form token, a laravel requirement
        COMMON.ajaxCsrfSetup();

		// Advertiser name
        $('.advertiser_name').on('click', function () {
       		//stop the carousel
			var carousel = Object.create(Carousel);
			carousel.stop();
            //checkIfLoggedIn($(this), ['admin', 'publisher', 'super_user']);
			// Check if can add advertiser
			checkIfAble2Add($(this));
        }).editable({
            placement: 'top',
            emptytext: 'ADVERTISER',
            params: function(params) {
				// Add data for ajax to pass on.
				if(TILECOLOR.advertiserHasColor(params.value.toLowerCase())) {
					params.color = TILECOLOR.getColorOfAdvertiser(params.value.toLowerCase());
				}
				// var _points = $(this).parents('.tile').find('#points').text();
				// var points = 0;
				// if(_points !== ''){
				// 	points = _points;
				// }
				params.points = $(this).parents('.tile').find('#points').text();
                params.publisher_id = $(this).parents('.tile').attr('data-publisher-id');
                return params;
            },
            validate: function(value) {
                return validate(value, $(this).parents('.tile'), false, false);
            },
            success: function(data, config) {
				var tile = $(this).parents('.tile');

				// Add class if long string
				if((config.split(' ').length) >= 3) $(this).addClass('long-str');
				else $(this).removeClass('long-str');

				// update color to tile
				tile.find('.name_wrap').css('background-color', data.advertiser.color)
									   .attr('data-color', data.advertiser.color)
							   		   .find('.tile_color').minicolors('destroy')
								   						   .val(data.advertiser.color)
									   					   .attr('data-defaultvalue', data.advertiser.color);

                occupied(tile, data);
				// Reinitialize colors
				if(data.advertiser.advertiser_name.toLowerCase() != 'engageiq') {
					initializeTileColor(tile, 'new');
				}

				updateMeter(data);
				TILECOLOR.updateAdvertiserColor(data.advertiser.advertiser_name.toLowerCase(), data.advertiser.color);
				console.log(TILECOLOR.getColors());
            }
        }).on('shown', function(e, editable) {
            onShown($(this).parents('.tile'), true);

			// transform text to bigger boobs
			$(this).parent().find('.input-medium').on('keyup', function() {
				var v = $(this).val();
			    $(this).val(v.toUpperCase());
			});

        }).on('hidden', function(e, reason){
            onHide($(this).parents('.tile'), true, false);
        });

		// Points
        $('.points').editable({
            placement: 'top',
            emptytext: 'POINTS',
            params: function(params) {
				// Add data for ajax to pass on.
                params.advertiser_name = $(this).parents('.tile').find('#advertiser_name').text();
                params.publisher_id = $(this).parents('.tile').attr('data-publisher-id');
                return params;
            },
            validate: function(value) {
                return validate(value, $(this).parents('.tile'), true, true);
            },
            success: function(data, config) {
                updateMeter(data);
                console.log(data);
                // if (data.errors == true) {
                // 	alert("you cant add it ");
                // }else{
                // 	updateMeter($data);
                // }
            }
        }).on('shown', function(e, editable) {
            onShown($(this).parents('.tile'), false);
            if(editable) showValue(editable);
        }).on('hidden', function(e, reason) {
            onHide($(this).parents('.tile'), false, true);
        });

		// Price
        $('.price').on('click', function () {
			// Check if inside frame, functionality for checking user as logged in triggered.
            //checkIfLoggedIn($(this), ['admin', 'publisher', 'super_user']);
            //stop the carousel
			//var carousel = Object.create(Carousel);
			//carousel.stop();
			// Check if can add advertiser
			//checkIfAble2Add($(this));
        }).editable({
            placement: 'top',
            emptytext: 'PRICE',
            params: function(params) {
				// Add data for ajax to pass on.
                params.publisher_id = $(this).parents('.tile').attr('data-publisher-id');
                return params;
            },
            validate: function(value) {
                return validate(value, $(this).parents('.tile'), false, true);
            },
            success: function(data, config) {
				var tile = $(this).parents('.tile');

                occupied(tile, data);
				initializeTileColor(tile, data.new);
            }
        }).on('shown', function(e, editable) {
            onShown($(this).parents('.tile'), true);
            //if(editable) showValue(editable);
        }).on('hidden', function(e, reason) {
            onHide($(this).parents('.tile'), false, true);
        });

		// Volune
        $('.volume').on('click', function () {
			// Check if inside frame, functionality for checking user as logged in triggered.
            //checkIfLoggedIn($(this), ['admin', 'publisher', 'super_user']);
            //stop the carousel
			//var carousel = Object.create(Carousel);
			//carousel.stop();
			// Check if can add advertiser
			//checkIfAble2Add($(this));
        }).editable({
            placement: 'top',
            emptytext: 'VOLUME',
            params: function(params) {
				// Add data for ajax to pass on.
                params.publisher_id = $(this).parents('.tile').attr('data-publisher-id');
                return params;
            },
            validate: function(value) {
                return validate(value, $(this).parents('.tile'), false, true);
            },
            success: function(data, config) {
				var tile = $(this).parents('.tile');

                occupied(tile, data);
				initializeTileColor(tile, data.new);
            }
        }).on('shown', function(e, editable) {
            onShown($(this).parents('.tile'), true);
            if(editable) showValue(editable);
        }).on('hidden', function(e, reason){
            onHide($(this).parents('.tile'), false, true);
        });
    };
	/*
	 * When the card is flipped, reset to 0 all points with advertiser name as "EngageIQ"
	 *
	 */
	var resetAllPoints = function() {
		// Destroy x-editable for easy update
		$('.editable').editable('destroy');

		// Loop through all occupied placement
		$('.advertiser.occupied').each(function() {
			var wrap = $(this).find('.name_wrap');
			var name_text = wrap.find('#advertiser_name').text();
			if(name_text.toLowerCase() == 'engageiq') {
					wrap.find('#points').text('0.00');
			}
		});

		// Reinitialize x-editable
		setEditable();
	};

	/*
	 * Public method.
	 *
	 */
    var publicMethod = {
        init: function() {
            setEditable();
        },
		resetPoints: function() {
            //resetAllPoints();
        },
		enableEdit: function() {
           setEditable();
        }
    }

    return publicMethod;

})(jQuery);

ADVERTISER.init();
