/*! 
 * TvProject - EngageIq
 * Tile color object
 * All action, event related to tile color were handled here.
 
 @class TILECOLOR
 @use Minicolors
 **/
var SPECTRUM = (function($) {
	/*
	 * Set options for mini colors.
	 *
	 */ 
	var options = {
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: 'top left',
        change: function(hex, opacity) {
            if (!hex) return;
            if (opacity) hex += ', ' + opacity;
            if (typeof console === 'object') {
            }
        },
        // theme: 'bootstrap',
        // format: 'hex',
        // swatches: ["#FF0000", "#FFFF00", "#008000", "#0000FF", "#800080", "#ffa500", "#4b0082", "#cccccc"],
        showPaletteOnly: true,
        togglePaletteOnly: true,
        togglePaletteMoreText: 'more',
        togglePaletteLessText: 'less',
        color: 'blanchedalmond',
        palette: [
            ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
            ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
            ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
            ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
            ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
            ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
            ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
            ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
        ],
        show: function(e) {
            $(this).parents('.tile').find('.name_wrap > div').css('z-index', 99999);
        },
        hide: function() {
            $(this).parents('.tile').find('.name_wrap > div').css('z-index', 1);
            input = {
                publisher_id: $(this).parents('.tile').data('publisher-id'),
                pk: $(this).parents('.tile').data('tile-placement'),
                name: 'color',
                value: $(this).val()
            };

			if($(this).parents('.tile').data('publisher-id')) {
				$.ajax({
					data: input,
					type: 'post',
					url: '/jeopardy/advertiser/pk-update',
					success: function(data) {
					},
					error: function (jqXHR, textStatus, errorThrown) {
						console.log(jqXHR.responseText);
					}
				});
			}
		}
    };
	
	/*
	 * Set the html entity.
	 *
	 * @param Element obj
	 */ 
    var htmlEntity = function (obj) {
          if(obj) return obj.find('.tile_color');
          return $('.tile_color');
    }
	
 	/*
	 * Public method.
	 *
	 */
    var publicMethod = {
        init: function(obj, remove) {
            var $tile_color = htmlEntity(obj);
            $tile_color.minicolors(options);

            if(remove) {
                $('div.advertiser[data-advertiser-id=""]').each(function(){
                      $(this).find('.tile_color').minicolors('destroy');
                });
            }
        }
    }

    return publicMethod;

})(jQuery);

SPECTRUM.init('', true);