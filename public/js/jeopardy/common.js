/*!
 * TvProject - EngageIq
 * Common object
 * All action, event with common use were handled here.

 @class COMMON
 @use Toastr
 @use Pulsate
 **/
var COMMON = (function($) {
	/*
	 * Flag to check if right section/points meter is shown
	 *
	 * @variable Bolean
	 */
    var isPointsMeterShown = false;

	/*
	 * Document ready, event listener.
	 *
	 */
    $(function(){
		// To show points meter/left section
		$('body')
		.on('click', '.toggle_meter', function () {
			//if(_showModalIfNotLoggedIn()) return;
			if(!isPointsMeterShown) showLeftSection();
			else hideLeftSection(false);
		});
		// Refresh icon is click
		$('#refresh_page').on('click', function() {
			  hideLeftSection(true);
		});
    });

	/*
	 * Series of action when left section/points meter is shown.
	 *
	 */
    var showLeftSection = function () {
          $('#rotator_wrap').animate({
            //position: 'relative', right:'-12%'
          },1000);

          $('#points_meter_wrap').css({left: '-800px', opacity: 'hide'})
                                 .animate({left:'5%',opacity:'show'}, 1000, function(){$('#show_meter').hide()});
          isPointsMeterShown = true;
    };

	/*
	 * Series of action when left section/points meter is to hide.
	 *
	 */
    var hideLeftSection = function (reload) {
        $('#rotator_wrap').animate({position: 'relative', right:'0%'},1000);
        $('#points_meter_wrap').animate({left:'-800px', opacity: 'hide'}, 1000,
                                            function(){
                                                if(reload) location.reload();
                                                $('#show_meter').show();
                                            }
                                        );
        isPointsMeterShown = false;
    };

	/*
	 * Check if to show login modal.
	 *
	 */
    var _showModalIfNotLoggedIn = function() {
        if( self != top) { // check id page is in iframe
            if(!parent.parent.modal()) return true;
        }
        return false;
    };

	/*
	 * Focus to a certain position.
	 *
	 */
    var scrollTo = function(el, offeset) {
        var pos = (el && el.size() > 0) ? el.offset().top : 0;

        if (el) {
            if ($('body').hasClass('page-header-fixed')) {
                pos = pos - $('.page-header').height();
            } else if ($('body').hasClass('page-header-top-fixed')) {
                pos = pos - $('.page-header-top').height();
            } else if ($('body').hasClass('page-header-menu-fixed')) {
                pos = pos - $('.page-header-menu').height();
            }
            pos = pos + (offeset ? offeset : -1 * el.height());
        }

        $('html,body').animate({
            scrollTop: pos
        }, 'slow');
    }

	/*
	 * genrate unique id.
	 *
	 */
    var getUniqueID = function(prefix) {
        return 'prefix_' + Math.floor(Math.random() * (new Date()).getTime());
    }

	/*
	 * Generate alert.
	 *
	 */
    var _alert = function(options) {

        options = $.extend(true, {
            container: "", // alerts parent container(by default placed after the page breadcrumbs)
            place: "append", // "append" or "prepend" in container
            type: 'success', // alert's type
            message: "", // alert's message
            close: true, // make alert closable
            reset: true, // close all previouse alerts first
            focus: true, // auto scroll to the alert after shown
            closeInSeconds: 0, // auto close after defined seconds
            icon: "" // put icon before the message
        }, options);

        var id = getUniqueID("App_alert");

        var html = '<div id="' + id + '" class="custom-alerts alert alert-' + options.type + ' fade in">' + (options.close ? '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>' : '') + (options.icon !== "" ? '<i class="fa-lg fa fa-' + options.icon + '"></i>  ' : '') + options.message + '</div>';

        if (options.reset) {
            $('.custom-alerts').remove();
        }

        if (!options.container) {
            if ($('.page-fixed-main-content').size() === 1) {
                $('.page-fixed-main-content').prepend(html);
            } else if (($('body').hasClass("page-container-bg-solid") || $('body').hasClass("page-content-white")) && $('.page-head').size() === 0) {
                $('.page-title').after(html);
            } else {
                if ($('.page-bar').size() > 0) {
                    $('.page-bar').after(html);
                } else {
                    $('.page-breadcrumb, .breadcrumbs').after(html);
                }
            }
        } else {
            if (options.place == "append") {
                $(options.container).append(html);
            } else {
                $(options.container).prepend(html);
            }
        }

        if (options.focus) {
            scrollTo($('#' + id));
        }

        if (options.closeInSeconds > 0) {
            setTimeout(function() {
                $('#' + id).remove();
            }, options.closeInSeconds * 1000);
        }

        return id;
    }

	/*
	 * Public method.
	 *
	 */
    var publicMethod = {
		/*
		 * Series of action when initializing.
		 *
		 */
        init: function() {
            var $self = this;

            toastr.options = {
                "closeButton": true,
                "positionClass": "toast-top-right",
            };
            $('.popovers').popover();
			      //$('.pulsate').pulsate({color: '#ed1a23'});

            if(parseFloat($('#points_wrap').text()) >= parseFloat($('#goal_wrap a').text())) {
                //$self.pulsateGoal();
            }

            //showLeftSection();
        },

		/*
		 * Reload.
		 *
		 */
        reload: function() {
            hideLeftSection(true);
        },

		/*
		 * Show login modal.
		 *
		 */
        showModalIfNotLoggedIn: function() {
            _showModalIfNotLoggedIn();
        },

		/*
		 * Pulsate card icon.
		 *
		 * @param Bolean num
		 */
        pulsateCardIcon : function (num) {
            var $self = this;
              if(!num) {
                  $('#get_cards').pulsate({color: '#ed1a23', glow: true });
				  // Fix colspan of table when empty details
                  $("#cards_table").on('show.bs.modal', function () {
                      $('.dataTables_empty').attr('colspan', '6');
                  });
              }
        },

		/*
		 * Pulsate clikc to draw button.
		 *
		 * @param Bolean pulsate
		 */
        alert : function (options, left_section) {
            var $self = this;
            if(left_section) showLeftSection();
            _alert(options);
        },

		/*
		 * Pulsate clikc to draw button.
		 *
		 * @param Bolean pulsate
		 */
        pulsateClick2DrawBtn : function (pulsate) {
            var $self = this;
            if(pulsate) {
                $('.percent_txt .pulsate').pulsate({color: '#ed1a23', glow: true });
                //     COMMON.alert({
                //     container: '#alert_container', // alerts parent container
                //     place: 'append', // append or prepent in container
                //     type: 'warning', // alert's type
                //     message: 'Please click on the pulsating button below to draw the card', // alert's message
                //     close: true, // make alert closable
                //     focus: true, // auto scroll to the alert after shown
                //     icon:'fa fa-warning' // put icon class before the message
                // }, false);
            }
        },

		/*
		 * Pulsate Goal number when goal is lesser or equal to points.
		 *
		 */
        pulsateGoal : function () {
            var $self = this;
              $('#goal_wrap a').pulsate({color: '#ed1a23', glow: true });
        },

		/*
		 * Animate points meter.
		 *
		 * @param Float pecent
		 */
        animateMeter: function(percent) {
            var $self = this;
            $('.skill .inner').animate({
                height: percent + '%'
            }, 1500, function() {
                $('.percent_txt').fadeIn().css('display', 'block');
            });
        },

		/*
		 * Check if data is numeric.
		 *
		 * @param Float data
		 */
        isNumeric: function( data ) {
            var $self = this;
            return !$.isArray( data ) && (data - parseFloat( data ) + 1) >= 0;
        },

		/*
		 * Clear the form fields value to empty.
		 *
		 * @param Object obj
		 */
        clearFields: function (obj) {
            var $self = this;
            $(obj).each(function () {
                if($(this).attr('name') != '_token') $(this).val('');
                $('#logo_display').attr('src', '').hide();
            });
        },

		/*
		 * Set the form attribute if adding or updating.
		 *
		 * @param Element form
		 * @param Integer placement
		 * @param String name
		 * @param String title
		 */
        setFormAttributes: function (form, placement, name, title) {
            var $self = this;
            $(form).attr('id', name + '_form').find('.form_trigger').attr('id', name + '_btn');
            $('#form #title_action').text(title);
            $('#form #tile_placement').val(placement).prop('readonly', true);
        },

		/*
		 * Get all fields value of form.
		 *
		 * @param Integer form_id
		 */
        getValues: function(form_id) {
            var $self = this;
              var values = {};
              $('#' + form_id + ' input, #' + form_id + ' textarea').each(function () {

                  if ($(this).attr('type') == 'checkbox') {
                      values[$(this).attr('name')] = $('input[name=' + $(this).attr('name') + ']:checked').map(function () {
                          return this.value;
                      }).get();
                  } else if ($(this).attr('type') == 'radio') {
                      values[$(this).attr('name')] = $('input[name=' + $(this).attr('name') + ']:checked').val();
                  } else {
                      values[$(this).attr('name')] = $(this).val();
                  }

              });

              return values;
        },

		/*
		 * Set xcrf token of ajax call.
		 *
		 */
        ajaxCsrfSetup: function() {
            var $self = this;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
        },

		/*
		 * Show the error of fields.
		 *
		 * @param Object obj
		 * @param String text
		 */
        showError: function(obj, text) {
            var $self = this;
              var message = '<label class="error font-red" style="font-size: 12px; padding: 4px 0px;" generated="true" for="';
               message += $(obj).attr('id') + '">' + text + '.</label>';
              $(obj).addClass('error');
              $(obj).next('label.error').remove();
              $( message ).insertAfter( obj );
        },

		/*
		 * Remove the error of fields.
		 *
		 * @param Object obj
		 */
        removeError: function(obj) {
            var $self = this;
              $( obj ).removeClass( 'error' );
              $(obj).next('label.error').remove();
        },

		/*
		 * Hide the modal.
		 *
		 * @param Object obj
		 */
        hideModal: function(obj) {
            var $self = this;
              $(obj).modal('hide');
        },

		/*
		 * Set message as toastr.
		 *
		 * @param String message
		 * @param String title
		 */
        message: function(message, title) {
            var $self = this;
              toastr.success(message, title);
        },

		/*
		 * Set message as toastr.
		 *
		 * @param String message
		 * @param String title
		 */
        messageWarning: function(message, title) {
            var $self = this;
              toastr.warning(message, title);
        },

		/*
		 * Set message as toastr.
		 *
		 * @param String message
		 * @param String title
		 */
        messageError: function(message, title) {
            var $self = this;
              toastr.error(message, title);
        },

		/*
		 * Set message as toastr.
		 *
		 * @param String message
		 * @param String title
		 */
        messageInfo: function(message, title) {
            var $self = this;
              toastr.info(message, title);
        }
    }

    return publicMethod;

})(jQuery);

COMMON.init();
