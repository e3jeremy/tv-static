/*!
 * TvProject - EngageIq
 * Logo object
 * All action, event related to logo were handled here.

 @class LOGO
 **/
var LOGO = (function($){
	/*
	 * Function on scanning the logo folder.
	 * Display the scan logo to logo manager  modal
	 *
	 */
	var logoManager = function (pass_filename) {

		var filemanager = $('.filemanager'),
			breadcrumbs = $('.breadcrumbs'),
			fileList = filemanager.find('.data');

		// Start by fetching the file data from scan.php with an AJAX request
		$.get('/jeopardy/scan', function(data, status, xhr) {
			var ct = xhr.getResponseHeader("content-type") || "";

			if (ct.indexOf('html') > -1) {
				var data = JSON.parse(data);
		    }

			var response = [data],
				currentPath = '',
				breadcrumbsUrls = [];

			var folders = [],
					files = [];

			// This event listener monitors changes on the URL. We use it to
			// capture back/forward navigation in the browser.

			$(window).on('hashchange', function(){

				goto(window.location.hash);

				// We are triggering the event. This will execute
				// this function on page load, so that we show the correct folder:

			}).trigger('hashchange');


			// Hiding and showing the search box

			filemanager.find('.search').click(function(){

				var search = $(this);

				search.find('span').hide();
				search.find('input[type=search]').show().focus();

			});


			// Listening for keyboard input on the search field.
			// We are using the "input" event which detects cut and paste
			// in addition to keyboard input.

			filemanager.find('input').on('input', function(e){

				folders = [];
				files = [];

				var value = this.value.trim();

				if(value.length) {

					filemanager.addClass('searching');

					// Update the hash on every key stroke
					value = value.replace(/ /g,"_").toLowerCase();
					window.location.hash = 'search=' + value.trim();
				}

				else {

					filemanager.removeClass('searching');
					window.location.hash = encodeURIComponent('');
					removeHash();

				}

			}).on('keyup', function(e){

				// Clicking 'ESC' button triggers focusout and cancels the search

				var search = $(this);

				if(e.keyCode == 27) {

					search.trigger('focusout');

				}

			}).focusout(function(e){

				// Cancel the search

				var search = $(this);

				if(!search.val().trim().length) {
				  //window.location.hash = encodeURIComponent(currentPath);
					search.hide();
					search.parent().find('span').show();

				}

			});


			// Clicking on folders

			fileList.on('click', 'li.folders', function(e){
				e.preventDefault();

				var nextDir = $(this).find('a.folders').attr('href');

				if(filemanager.hasClass('searching')) {

					// Building the breadcrumbs

					breadcrumbsUrls = generateBreadcrumbs(nextDir);

					filemanager.removeClass('searching');
					filemanager.find('input[type=search]').val('').hide();
					filemanager.find('span').show();
				}
				else {
					breadcrumbsUrls.push(nextDir);
				}

				window.location.hash = encodeURIComponent(nextDir);
				currentPath = nextDir;

			});


			// Clicking on breadcrumbs

			breadcrumbs.on('click', 'a', function(e){
				e.preventDefault();

				var index = breadcrumbs.find('a').index($(this)),
					nextDir = breadcrumbsUrls[index];

				breadcrumbsUrls.length = Number(index);

				window.location.hash = encodeURIComponent(nextDir);

			});


			// Navigates to the given hash (path)

			function goto(hash) {

				hash = decodeURIComponent(hash).slice(1).split('=');

				if (hash.length) {
					var rendered = '';

					// if hash has search in it

					if (hash[0] === 'search') {

						filemanager.addClass('searching');
						rendered = searchData(response, hash[1].toLowerCase());

						if (rendered.length) {
							currentPath = hash[0];
							render(rendered);
						}
						else {
							render(rendered);
						}

					}

					// if hash is some path

					else if (hash[0].trim().length) {

						rendered = searchByPath(hash[0]);

						if (rendered.length) {

							currentPath = hash[0];
							breadcrumbsUrls = generateBreadcrumbs(hash[0]);
							render(rendered);

						}
						else {
							currentPath = hash[0];
							breadcrumbsUrls = generateBreadcrumbs(hash[0]);
							render(rendered);
						}

					}

					// if there is no hash

					else {
						currentPath = data.path;
						breadcrumbsUrls.push(data.path);
						render(searchByPath(data.path));
					}
				}
			}

			// Splits a file path and turns it into clickable breadcrumbs

			function generateBreadcrumbs(nextDir){
				var path = nextDir.split('/').slice(0);
				for(var i=1;i<path.length;i++){
					path[i] = path[i-1]+ '/' +path[i];
				}
				return path;
			}


			// Locates a file by path

			function searchByPath(dir) {
				if(dir) {
						var path = dir.split('/'),
							demo = response,
							flag = 0;

						for(var i=0;i<path.length;i++){
							for(var j=0;j<demo.length;j++){
								if(demo[j].name === path[i]){
									flag = 1;
									demo = demo[j].items;
									break;
								}
							}
						}

						demo = flag ? demo : [];
						return demo;
				}
				return [];
			}


			// Recursively search through the file tree

			function searchData(data, searchTerms) {

				data.forEach(function(d){
					if(d.type === 'folder') {

						searchData(d.items,searchTerms);

						if(d.name.toLowerCase().match(searchTerms)) {
							folders.push(d);
						}
					}
					else if(d.type === 'file') {
						if(d.name.toLowerCase().match(searchTerms)) {
							files.push(d);
						}
					}
				});
				return {folders: folders, files: files};
			}


			// Render the HTML for the file manager

			function render(data) {

				var scannedFolders = [],
					scannedFiles = [];

				if(Array.isArray(data)) {

					data.forEach(function (d) {

						if (d.type === 'folder') {
							scannedFolders.push(d);
						}
						else if (d.type === 'file') {
							scannedFiles.push(d);
						}

					});

				}
				else if(typeof data === 'object') {

					scannedFolders = data.folders;
					scannedFiles = data.files;

				}


				// Empty the old result and make the new one

				fileList.empty().hide();

				if(!scannedFolders.length && !scannedFiles.length) {
					filemanager.find('.nothingfound').show();
				}
				else {
					filemanager.find('.nothingfound').hide();
				}

				if(scannedFolders.length) {

					scannedFolders.forEach(function(f) {

						var itemsLength = f.items.length,
							name = escapeHTML(f.name),
							icon = '<span class="icon folder"></span>';

						if(itemsLength) {
							icon = '<span class="icon folder full"></span>';
						}

						if(itemsLength == 1) {
							itemsLength += ' item';
						}
						else if(itemsLength > 1) {
							itemsLength += ' items';
						}
						else {
							itemsLength = 'Empty';
						}
						var folder_html ='';
								folder_html = '<div class="file-box">';
							//folder_html = '<li class="folders">';
								folder_html += '<a href="'+ f.path +'" title="'+ f.path +'" class="folders">';
								folder_html +=icon;
								folder_html +='<span class="name">';
								folder_html +=name;
							folder_html +='</span> <span class="details">';
								folder_html +=itemsLength;
							  folder_html +='</span></a>';
								//folder_html +='</li>';
								folder_html +='</div>';
						var folder = $(folder_html);
						//folder.appendTo(fileList);
					});

				}

				if(scannedFiles.length) {

					scannedFiles.forEach(function(f) {

						var fileSize = bytesToSize(f.size),
							name = escapeHTML(f.name),
							_file = name.split('.'),
							icon = '<span class="icon file"></span>';

						var fileType = _file[_file.length-1];

						if (fileType == "jpg" || fileType == "png" || fileType == "jpeg" || fileType == "gif") {
							var name = f.name.split('.');
							icon = '<div class="files_thumb" id="' + name[0] + '" data-path="' + f.path + '" style="background: url(\'/' + f.path + '\')  no-repeat center center; background-size: 75% auto; height: 130px;"></div>';
						} else {

							icon = '<span class="icon file f-'+ fileType +'">.'+ fileType +'</span>';
						}
						if(fileType != "zip") {
							var file = $(gridLayout(f, _file[_file.length-2], icon, fileSize));

							file.appendTo(fileList);
						}
					});

				}


				// Generate the breadcrumbs

				var url = '';

				if(filemanager.hasClass('searching')){

					url = '<span>Search results: </span>';
					//fileList.removeClass('animated');

				}
				else {

					//fileList.addClass('animated');

					breadcrumbsUrls.forEach(function (u, i) {

						var name = u.split('/');

						//if (i !== breadcrumbsUrls.length - 1) {
						//	url += '<a href="'+u+'"><span class="folderName">' + name[name.length-1] + '</span></a> <span class="arrow">→</span> ';
						//}
						//else {
							url = '<span class="folderName">' + name[name.length-1] + '</span>';
						//}

					});

				}

				breadcrumbs.text('').append(url);


				// Show the generated elements

				//fileList.animate({'display':'inline-block'});
				fileList.show('slow');

			}


			// This function escapes special html characters in names

			function escapeHTML(text) {
				return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
			}


			// Convert file sizes from bytes to human readable units

			function bytesToSize(bytes) {
				var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
				if (bytes == 0) return '0 Bytes';
				var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
				return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
			}


			// Convert file sizes from bytes to human readable units

			function gridLayout(item, name, icon, fileSize) {
				var html = '';
					html += '<div class="file-box">';
					html += '<div class="close delete_logo" data-logo-path="' + item.path + '"> x </div>';
					//html += '<a href="'+ item.path+'" title="'+ item.path +'" class="files">';
					html += icon;
					html += '<span class="name">'+ name.replace(/_/g, ' ') +'</span> <span class="details">';
					html += fileSize;
					html += '</span>';
					//html += '</a>';
					html += '<div class="portlet-body">&nbsp;';
					html += '<div>';
					html += '</div>';

				return html;
			}

				// Remove hash in url
			function removeHash () {
					_removeHash();
				}

		}).always(function() {
            if(pass_filename) window.location.hash = 'search=' + pass_filename.trim();
            if(pass_filename) $('#search_logo').val(pass_filename).show();
            $('#start_upload').hide();
            $('#fileupload-name, #progress').empty();
        });
	};

	/*
	 * Remove the hashtag in url when search is empty
	 *
	 */
	var _removeHash = function () {
		var scrollV, scrollH, loc = window.location;
		if ("pushState" in history)
				history.pushState("", document.title, loc.pathname + loc.search);
		else {
				// Prevent scrolling by storing the page's current scroll offset
				scrollV = document.body.scrollTop;
				scrollH = document.body.scrollLeft;

				loc.hash = "";

				// Restore the scroll offset, should be flicker free
				document.body.scrollTop = scrollV;
				document.body.scrollLeft = scrollH;
		}
	};
	/* END OF LOGO MANAGER */


	/*
	 * Flag to check if to continue to update as ajax call.
	 *
	 * @variable Bolesn
	 */
    var ajax_update = false;

	/*
	 *
	 * @variable String
	 */
	var publisher_id, tile_placement, file, filename;

	/*
	 * This is the container for image html when click.
	 * Use as reference for deleting logos.
	 *
	 * @variable String
	 */
	var img_wrap_active;

	/*
	 * Document ready, event listener.
	 *
	 */
	$(function(){
		/*
		 * Open logo manager in publisher form.
		 *
		 */
        $('#call_logo_manager').on('click', function(){
			img_wrap_active  = $(this);
			console.log(img_wrap_active.attr('id'));

            $('#logo_manager_wrap').show('slow');
            $('#logo_manager').find('.modal-dialog')
							   .css({'width': '62%', 'margin': '3% 0 0 30%'})
								.find('.data').show();
			_removeHash();
            ajax_update = false;
        });

		/*
		 * Open logo manager by advertiser logo.
		 *
		 */
        $('.jeopardy_board').on('click', '.logo_wrap h3, .logo_wrap img', function(){
        	//stop the carousel
			var carousel = Object.create(Carousel);
			carousel.stop();
			
			img_wrap_active = $(this);

			//if(!IsLoggedIn(['admin', 'publisher', 'super_user'])) return;

			if(!$(this).parents('.tile').attr('data-publisher-id')) {
				COMMON.message('Add publisher first.', 'Unable To Add');
				return;
			}

			$('#logo_manager').modal('show');
			publisher_id = $(this).parents('.tile').attr('data-publisher-id');
			tile_placement = $(this).parents('.tile').attr('data-tile-placement');
			ajax_update = true;
        });

		/*
         * insert logo to form
		 *
		 */
		$('div.data').on('click', '.files_thumb', function(){
            insertLogo2Form($(this).data('path'));
		});

		/*
         * Catch the filename after slelected from window dialouge
		 *
		 */
        $(":file").change(function(){
            var _file = $(":file").val();
            var fileNameIndex = _file.lastIndexOf("\\") + 1;

            filename = _file.substr(fileNameIndex);
            file = filename.split('.');

            $('#start_upload').show();
            $('#fileupload-name').text(filename);
        });

		/*
         * Submit button triggered
		 *
		 */
        $('#upload_logo_form').submit(
            function (event) {
                event.preventDefault();
                upload(new FormData(this));
            }
        );

		/*
		 * Series of action when modal is hidden
		 *
		 */
        $('#logo_manager').on('hidden.bs.modal', function () {
            $('#search_logo').val('');
			window.location.hash = encodeURIComponent('');
			_removeHash();

            $('#logo_manager').find('.modal-dialog').css({'width': '87%', 'margin': '3% auto 0'});
            $('.tile').removeClass('clicked');
        });

		/*
		 * Delete logo
		 *
		 */
		$('#logo_manager').on('click', '.delete_logo', function() {
			var input = {
				'path': $(this).data('logo-path')
			};

			COMMON.ajaxCsrfSetup();

             $.ajax({
                data: input,
                type: 'post',
                url: '/jeopardy/logo/delete',
                success: function (result) {
					   	logoManager('');
						$('.publisher-wrap img').each(function() {
							if($(this).attr('src') == '/' + input.path) {
								$(this).parent().html('<h2 class="add_publisher">Publisher<br> Logo</h2>');
								$('.inner_circle #logo_display').attr('src', '');
							}
						});
						$('#logo_display_pyramid').each(function() {
							if($(this).attr('src') == '/' + input.path) {
								$(this).attr('src', '').hide().parent().find('h3').show();
							}
						});

	   					COMMON.message('You have successfully Deleted the selected logo.', 'Deleted Logo!');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR.responseText);
                }
            });
		});
    });

	/*
	 * Show pop up login modal if not login.
	 * If not inside iframe, login modal is not rquired.
	 *
	 * @param Array/Object roles
	 */
	var IsLoggedIn = function (roles) {
		// If inside iframe
		if( self != top) {
			// Check if its already login, if not a login modal will show.
			if(!parent.parent.modal()){
				return false;
			}
			// Already login
			else {
				// Check user has the ability to edit.
				if(!ROLES.hasAbility2Edit(roles)) {
					COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
					return false;
				}
			}
		}
		// If the page is not iframe.
		else {
			// Check user has the ability to edit.
			if(!ROLES.hasAbility2Edit(roles)) {
				COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
				return false;
			}
		}
		return true;
	}

	/*
	 * Insert the selected logo to form
	 *
	 * @param String path
	 */
    var insertLogo2Form = function(path) {
        $('#publisher_logo').val(path).show();
        $('#logo_manager').modal('hide');
        $('#logo_display').attr('src', '/' + path).show();
        $('#logo_manager_wrap').fadeOut('slow');

        if(ajax_update) {
            var input = {
                publisher_id: publisher_id,
                pk: tile_placement,
                name: 'advertiser_logo',
                value: path
            };

            COMMON.ajaxCsrfSetup();

             $.ajax({
                 data: input,
                 type: 'post',
                 url: '/jeopardy/advertiser/pk-update',
                 success: function (result) {
                       //publishers[result.publisher.tile_placement] = result.publisher;

                       var $wrap = $('div[data-tile-placement=' + result.advertiser.tile_placement + '][data-publisher-id=' + result.advertiser.publisher_id + ']');
                       $wrap.addClass('occupied').find('img').attr('src', '/' + result.advertiser.advertiser_logo).show().parent().find('h3').hide();
                 },
                 error: function (jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR.responseText);
                 }
             });
        }
    };

	/*
	 * Upload logo
	 *
	 * @param Object form_data
	 */
    var upload = function(form_data) {
         COMMON.ajaxCsrfSetup();

        $.ajax({
          url: '/jeopardy/logo/upload',
          xhr: function() {
               var xhr = new XMLHttpRequest();
               var total = 0;

               // Get the total size of files
               $.each(document.getElementById('file').files, function(i, _file) {
                      total += _file.size;
               });

               // Called when upload progress changes. xhr2
               xhr.upload.addEventListener("progress", function(evt) {
                      // show progress like example
                      var loaded = (evt.loaded / total).toFixed(2)*100; // percent

                      $('#progress').text('Uploading... ' + loaded + '%' );
               }, false);

               return xhr;
          },
          type: 'post',
          processData: false,
          contentType: false,
          data: form_data,
          success: function(data) {
                  logoManager(data);
                  COMMON.message('You have successfully upload a new logo.', 'Upload Logo');
            }
        });
    };

	/*
	 * Create drag and drop for upload
	 *
	 */
	var dropzone = function () {
		if (document.getElementById('logo-manager-dropzone')) {
	        var myDropzone = new Dropzone("#logo-manager-dropzone", { url: "/jeopardy/upload_logo"});

	        myDropzone.on("sending", function(file, xhr, formData) {
	            formData.append("_token", $('meta[name="_token"]').attr('content'));
	        }).on("success", function(file, response) {
	            $('.dz-image-preview').remove();
	            logoManager(response);
	        });
	    }
	}

	return {
			init: function(name) {
				  logoManager(name);
			},
			iniDropzone: function () {
				dropzone();
			}
	}
})(jQuery);

LOGO.init('');
LOGO.iniDropzone();
