<?php

Route::group(['middleware' => 'auth.basic'], function () {

    Route::post('/webhook/github', 'GitHubWebhookController@gitRepoReceivedPush');
    Route::get('/', 'DashboardController@index');
    Route::get('/widgets/admin/getAllContent', 'AdminWidgetController@index');
    Route::get('/widgets/feedback/getAllContent', 'FeedbackWidgetController@index');

    /*-- Routes for Jeopardy in group --*/
    Route::group(array('prefix' => 'jeopardy'), function () {
        // View display
        Route::get('/', 'JeopardyController@index');
        // Scan ulpoad images for modal view.
        Route::get('/scan', 'JeopardyController@scan');
        // Ajax Call
        Route::post('/card/{method}', 'JeopardyController@card');
        Route::post('/publisher/{method}', 'JeopardyController@publisher');
        Route::post('/advertiser/{method}', 'JeopardyController@advertiser');
        Route::post('/global/{method}', 'JeopardyController@globalSetting');
        Route::post('/points-meter/{method}', 'JeopardyController@pointsMeter');

        // any method
        Route::any('/logo/{method}', 'JeopardyController@logo');
        Route::any('/upload_logo', 'JeopardyController@uploadLogo'); // this should be include in above route
        Route::any('/test/{method}', 'JeopardyController@test');
        Route::any('/migration/{method}', 'JeopardyController@migration');

        Route::get('/testroles', 'JeopardyController@testCheckIfHasRoles');

        Route::get('/test', function () {
            $cards =CardsAndEvents::getDrawnCardToday();
            foreach ($cards as $card) {
                echo $card->description;
            }
        });

        Route::get('/points-meter/get-card-reached-goal', 'JeopardyController@getCardReachedGoal');
    });
});
Route::get('/pusher', function () {
    try {
        event(new \App\Events\Feedback\FeedbackUpdated('hi there'));
        return 'event has been sent';
    } catch (Exception $e) {
        echo $e->getMessage();
    }
});
