<?php

namespace App\Jeopardy\Entities;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'discription', 'is_flipped', 'status'
    ];
}
