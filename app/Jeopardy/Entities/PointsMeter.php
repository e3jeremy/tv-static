<?php

namespace App\Jeopardy\Entities;

use Illuminate\Database\Eloquent\Model;

class PointsMeter extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'points', 'goal', 'card_flipped', 'card_id', 'instance', 'incrementer'
    ];

	/**
	 * Get the publisher that owns the event.
	 */
	public function card()
	{
		return $this->hasOne('App\Jeopardy\Entities\Card', 'id', 'card_id');
	}
}
