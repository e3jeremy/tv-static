<?php

namespace App\Jeopardy\Entities;

use Illuminate\Database\Eloquent\Model;

class Advertiser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'publisher_id', 'advertiser_name', 'advertiser_logo', 'price', 'volume', 'points','tile_placement'
    ];

    public function publisher()
    {
        return $this->belongsTo(\App\Jeopardy\Entities\Publisher::class);
    }
}
