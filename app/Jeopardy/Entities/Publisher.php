<?php

namespace App\Jeopardy\Entities;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'publisher_name', 'placement', 'client_name', 'publisher_logo', 'placement_company', 'price', 'volume', 'tile_placement'
    ];

    public function advertisers()
    {
        return $this->hasMany(\App\Jeopardy\Entities\Advertiser::class);
    }
}
