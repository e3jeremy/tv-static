<?php

namespace App\Jeopardy\Entities;

use Illuminate\Database\Eloquent\Model;

class BoardSetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table ='board_settings_static';
    protected $fillable = [
       'scope', 'key', 'value'
    ];
}
