<?php

namespace App\Jeopardy\Services\Mail;

use Mail;
use Config;

/**
 * the mail class for Jeopardy Module
 */
trait Email
{
    public function updatePointsNotification()
    {
        try {
            Mail::send('email.points_meter_notification', [], function ($message)
            {
                $notification = Config::get('jeopardy.points_meter_notification');

                $message->from($notification['from'], $notification['from_name'])
                        ->subject($notification['subject'])
                        ->to($notification['to'])
                        ->cc($notification['cc']);
            });
            return response()->json(['message' => 'Request completed']);
        } catch (\Exception $e) {
            return $e . " not sending email";
        }
    }
    /**
     *send email if 'publisher:deletezerotileplacement' command is run
     * Cron Job
     * @var resource
     */
    public function deletePublisherNotification()
    {
        try {
            Mail::send('email.delete_publisher_notification', [], function ($message) {
                $notification = Config::get('jeopardy.delete_publisher_notification');
                $message->from($notification['from'], $notification['from_name'])
                    ->subject($notification['subject'])
                    ->to($notification['to'])
                    ->cc($notification['cc']);
            });
            return response()->json(['message' => 'Request completed']);
        } catch (\Exception $e) {
            return $e . " not sending email";
        }
    }
    public function cardFlippedNotification()
    {
        try {
            Mail::send('email.card_flipped_notification', [], function ($message) {
                $notification = Config::get('jeopardy.card_flipped_notification');
                $message->from($notification['from'], $notification['from_name'])
                    ->subject($notification['subject'])
                    ->to($notification['to'])
                    ->cc($notification['cc']);
            });
            return response()->json(['message' => 'Request completed']);
        } catch (\Exception $e) {
            return $e . " not sending email";
        }
    }
}
