<?php
namespace App\Jeopardy\Services;

final class ScanFile
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    /**
     * Location of folder to scan.
     *
     */
    protected $dir;

    /**
     * Content type for response.
     *
     */
    protected $content_type;

    /**
     * File type to be scanned.
     *
     */
    protected $type = 'folder';

    /**
     * Construct default.
     *
     */
    public function __construct($dir = 'logos')
    {
        $this->setDir($dir);
        $this->setContentType('application/json');
    }

    /**
     * Set the content type.
     *
     * @param string $content_type
     */
    public function setContentType($content_type)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $content_type);

        $this->content_type = $content_type;
    }

    /**
     * Set the location directory.
     *
     * @param string $dir
     */
    public function setDir($dir)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $dir);

        $this->dir = $dir;
    }

    /**
     * Run the recursive function.
     * Output the directory listing as JSON
     *
     */
    public function run()
    {
        $response = $this->scan($this->dir);

        header('Content-type: ' . $this->content_type);

        echo json_encode(array(
                  'name' => $this->dir,
                  'type' => $this->type,
                  'path' => $this->dir,
                  'items' => $response
            ));
    }

    /**
    * This function scans the files folder recursively, and builds a large array
    *
    * @param string $dir
    */
    protected function scan($dir)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $dir);

        $files = array();

        // Is there actually such a folder/file?
        if (file_exists($dir)) {
            foreach (scandir($dir) as $f) {
                if (!$f || $f[0] == '.') {
                    continue; // Ignore hidden files
                }

                if (is_dir($dir . '/' . $f)) {
                    // The path is a folder
                $files[] = [
                        "name" => $f,
                        "type" => "folder",
                        "path" => $dir . '/' . $f,
                        "items" => $this->scan($dir . '/' . $f) // Recursively get the contents of the folder
                    ];
                } else {
                    // It is a file
                    $files[] = [
                            "name" => $f,
                            "type" => "file",
                            "path" => $dir . '/' . $f,
                            "size" => filesize($dir . '/' . $f) // Gets the size of this file
                        ];
                }
            }
        }

        return $files;
    }
}
