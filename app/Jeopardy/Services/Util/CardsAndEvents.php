<?php
namespace App\Jeopardy\Services\Util;

class CardsAndEvents
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

	protected $cards = array();
	protected $cards_events = array();
   
    /**
    * Set all queried card to variable
    *
    * @param Array $cards
     */
	public function setCards($cards)
	{
		$this->cards = $cards;
	}

    /**
    * Get all cards
    *
    * @return Array
     */
	public function getCards()
	{
		return $this->cards;
	}

    /**
    * Count all cards
    *
    * @return Integer
     */
	public function countCards()
	{
		return count($this->cards);
	}

	/**
    * Set all queried card event to variable
    *
    * @param Array $cards_events
     */
	public function setCardsEvents($cards_events)
	{
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $cards_events);

		$this->cards_events = $cards_events;
	}

    /**
    * Get all cards events
    *
    * @return Array
     */
	public function getCardsEvents()
	{
		return $this->cards_events;
	}

    /**
    * Count all cards events
    *
    * @return Integer
     */
	public function countCardsEvents()
	{
		return count($this->cards_events);
	}
    /**
     *the card that was drawn today
     */
    public function getDrawnCardToday()
    {
        $card = new \App\Jeopardy\Repositories\Eloquent\Card(new \App\Jeopardy\Entities\Card);
        return $card->getDrawnCardToday();
    }
}

