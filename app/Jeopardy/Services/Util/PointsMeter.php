<?php
namespace App\Jeopardy\Services\Util;

use DB;
use Config;
use BoardSetting;

use App\Jeopardy\Services\Advertisers;
use App\Jeopardy\Services\Mail\Email as Email;
use App\Jeopardy\Services\Message\Message as Message;

class PointsMeter extends PointsMeterCalculator
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;
    use Email;
    use Message;
    /**
     * Variable to either update or not the points meter.
     *
     * @var string
     */
    public $update_points_meter = 0;

    /**
     * Variable container for updating the points.
     *
     * @var integer
     */
    public $addend = '';
    private $advertisers;

    /**
     * instantiate and inject model
     *
     * @param Model $card
     * @return void
     */
    public function __construct(\App\Jeopardy\Repositories\Contracts\PointsMeterRepository $pointsmeter)
    {
        $this->points_meter = $pointsmeter;
    }

    /**
     * Get all cards events with selected fields.
     *
     * @param array $field
     * @return object
     */
    public function selectAllEventsWithFields($field)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $field);

        return $this->points_meter->select($field);
    }

    /**
     * Get all cards.
     *
     * @return array
     */
    public function selectAll()
    {
        return $this->points_meter->all();
    }

    /**
     * Add new points meter event.
     *
     * @param array $data
     * @return array
     */
    public function create($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        if(strtolower($data['advertiser_name']) == 'engageiq') {
            $insert = ['points' => $data['points'], 'goal' => BoardSetting::getGlobalGoal(), 'instance' => 1];
            $this->points_meter->add([ 'card_flipped' => 0], $insert);
        }

        return array(
                'errors' => false
            );
    }

    /**
     * Update points meter.
     *
     * @param object $data
     * @return array
     */
    public function pkUpdate($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'object' => true], $data);

        $flipped = false;
        $this->points_meter->updateOrCreate($data->toArray());

        return array(
                'errors' => false,
                'points' => $this->getPoints(),
                'goal' => $this->getGoal(),
                'percent' => $this->getPercent()
            );
    }

    /**
     * Update advertiser by x-editable form.
     *
     * @param object $data
     * @return array
     */
    public function updateTitle($data)
    {
        // Validate arguments and throw errors
        $this->points_meter->updateTitle(['id' => $data['pk'], 'card_title' => $data['value']]);

        return array(
                'errors' => false,
                'id' => $data['pk'],
                'title' => $data['value']
            );
    }

    /**
     * Update advertiser by x-editable form.
     *
     * @param object $data
     * @return array
     */
    public function updateDescription($data)
    {
        // Validate arguments and throw errors
        $this->points_meter->updateDescription(['id' => $data['pk'], 'card_description' => $data['value']]);

        return array(
                'errors' => false,
                'id' => $data['pk'],
                'description' => $data['value']
            );
    }

    /**
     * Update points meter event.
     *
     * @param array $data
     * @return void
     */
    public function updateCardflippedStatus($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        return $this->points_meter->updateCardflipped($data);
    }

    /**
     * Update points.
     *
     * @param string|integer $addend
     * @return array
     */
    public function updatePoints($addend)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true], $addend);

        return $this->points_meter->updatePoints($addend);

        return array(
                'errors' => false
            );
    }
    public function cronUpdatePointsMeter()
    {
        //$engageiq = 'ENGAGEIQ';
        //$total = DB::table('advertisers')->where('advertiser_name', $engageiq)->orWhere('advertiser_name','engageiq')->sum('points');

        $total = Advertisers::addAllPointsOfEngageIq();

        if ($this->getPoints() < $this->getGoal()) {
            $this->updatePoints($total);
        } else {
            $this->flipTheCard();
        }
        return $total;
    }
    /**
     * Force update points.
     *
     * @param array $advertisers
     * @return array
     */
    public function forceUpdatePoints($advertisers)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'array' => true], $advertisers);

        $total = 0;
        foreach($advertisers as $group => $_advertisers) {
            foreach($_advertisers as $index => $advertiser) {
                if(strtolower($advertiser['advertiser_name']) == 'engageiq') $total += $advertiser['points'];
            }
        }

        $this->points_meter->updatePointsBrute($total);

        return array(
                  'errors' => false
                );
    }

    /**
     * Update points meter event.
     *
     * @param array $data
     * @return void
     */
    public function createNewInstance($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        $points = Advertisers::addAllPointsOfEngageIq();
        //$insert = ['points' => $data['points'], 'goal' => $data['goal'] + BoardSetting::getGlobalGoalInterval(), 'instance' => $data['instance'] + 1];
        $insert = ['points' => $points, 'goal' => $data['goal'], 'instance' => $data['instance'] + 1];
        $this->points_meter->add([ 'card_flipped' => 0], $insert);

        //return  ($data['points'] / ($data['goal'] + $setting->getGlobalGoalInterval())) * 100;
    }

    /**
     * Delete card.
     *
     * @param integer $id
     * @return array
     */
    public function delete($data)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'integer' => true], $id);

        $this->points_meter->delete($data['id']);

        return array(
                  'errors' => false
                );
    }

    /**
     * Get the Points.
     *
     * @param array $data
     * @return array
     */
    public function getPoints()
    {
        $data = $this->points_meter->getActive();

        return ($data) ? $data['points'] : 0;
    }

    /**
     * Get the Goal.
     *
     * @param array $data
     * @return array
     */
    public function getGoal()
    {
        $data = $this->points_meter->getActive();

        return ($data) ? $data['goal'] : ($data['goal'] > 0) ? $data['goal'] : 100;
    }

    /**
     * Get the Instance.
     *
     * @param integer $operation
     * @return array
     */
    public function getInstance($operation = 0)
    {
        $data = $this->points_meter->getActive();

        return ($data) ? $data['instance'] + ($operation) : 0;
    }

    /**
     * Get the Percent.
     *
     * @param array $data
     * @return array
     */
    public function getPercent()
    {
        $data = $this->points_meter->getActive();

        if($data && $data['goal'] != 0) return number_format(($data['points'] / $data['goal']) * 100, 1, '.', '');
        else return 0;
    }

    /**
     * Check gaol vs points.
     *
     * @param object $settings
     * @return array
     */
    public function pointsExceedGoal()
    {
        if($this->getPercent() >= 100) return true;
        return false;
    }

    /**
     * Set data for updating points.
     *
     * @param array $existing_data
     * @param array $data
     * @return Bolean
     */
    public function updatePointsMeter($existing_data, $data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $existing_data);
        $this->dataMust(['required' => true, 'array' => true], $data);

        extract($data);
        if ($name === "advertiser_name") {
            $value = strtolower($value);
        }
        if($name == 'points' || $name == 'advertiser_name') {
            if($name == 'points') $data = ['points' => (float)$value, 'advertiser_name' => $advertiser_name];
            if($name == 'advertiser_name') $data = ['points' => (float)$points, 'advertiser_name' => $value];
            $this->setVariablesInUpdatingpointsMeter ($existing_data, $data);
        }
        return $data;
    }
    /**
     * flipped card
     * @return resource
     * type Object
     */
    public function getDrawnCard()
    {
        return $this->points_meter->getDrawnCard();
    }
    /**
     * flipped card, but no notification sent via email
     * @var resource
     */
    public function getDrawnCardNotNotified()
    {
        $drawnCardId = $this->points_meter->getDrawnCardNotNotified();
        if ($drawnCardId) {
            //send email notification
            $this->cardFlippedNotification();
            $this->points_meter->updateDrawnCardNotNotified($drawnCardId);
        }
    }
    /**
     * flipped the card if goal is reached
     */
    public function flipTheCard()
    {
        $goalReachedCardId = $this->points_meter->getCardReachedGoal();
        $card = new \App\Jeopardy\Services\Cards(new \App\Jeopardy\Repositories\Eloquent\Card(new \App\Jeopardy\Entities\Card));
        if ($goalReachedCardId) {
            //$this->points_meter->flipTheCard($goalReachedCardId);
           $card->getRandomCard();
        }
        return $goalReachedCardId;
    }
    public function getCardReachedGoal()
    {
        return $this->points_meter->getCardReachedGoal();
    }
}
