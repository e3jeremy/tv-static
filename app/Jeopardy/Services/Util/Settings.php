<?php
namespace App\Jeopardy\Services\Util;

use App\Jeopardy\Repositories\Contracts\SettingRepository;

use App\Jeopardy\Helpers\SettingHelper;

class Settings
{
    /**
     * @const int global settings.
     *
     */
    const max_rows= 10;
    const goal_interval = 0;
    const deducted_rows = 1;
    const value_per_hour = 0;
    const default_goal = 100;
    const column_per_slide = 6;
    const row_per_publisher = 7;
    const advertiser_visible = 7;


    /**
     * Instantiate and inject model.
     *
     * @param Model $setting
     * @return void
     */
    public function __construct(SettingRepository $setting)
    {
        $this->setting = $setting;
        $this->setGlobalSettings();
    }

    /**
     * Set global settings.
     *
     * @return void
     */
    public function setGlobalSettings()
    {
        $settings = $this->setting->where(array('scope' => 'global'));

        if($settings) {
            foreach($settings as $setting) {
                $var = str_replace('global_', '', $setting['key']);
                 $this->$var = $setting['value'];
            }
        }
    }

    /**
     * Get global settings.
     *
     * @return array|void
     */
    public function getGlobalSettings()
    {
        return $this->setting->where(array('scope' => 'global'));
    }

    /**
     * Check this object has property.
     *
     * @param string $roperty
     * @return bolean
     */
    public function hasProperty($roperty)
    {
        return property_exists($this, $roperty);
    }

    /**
     * Set global settings for value per day.
     *
     * @return integer
     */
    public function getGlobalValuePerDay()
    {
        return ($this->hasProperty('value_per_hour')) ? $this->value_per_hour : self::value_per_hour;
    }

    /**
     * Set global settings for goal interval.
     *
     * @return integer
     */
    public function getGlobalGoalInterval()
    {
        return ($this->hasProperty('goal_interval')) ? $this->goal_interval : self::goal_interval;
    }

    /**
     * Get the default goal.
     *
     * @return integer
     */
    public function getDefaultGoal()
    {
        return ($this->hasProperty('default_goal')) ? $this->default_goal : self::default_goal;
    }

    /**
     * Get global settings for goal.
     *
     * @param integer $instance
     * @return integer
     */
    public function getGlobalGoal($instance = 1)
    {
        return ($this->hasProperty('goal_' . $instance)) ? $this->{'goal_' . $instance} : $this->getDefaultGoal();
    }

    /**
     * Add Global.
     *
     * @param object $data
     * @return array
     */
    public function addGlobal($data)
    {
         $global = $this->setting->add($data->toArray(), 'global');

         return ['global' => $global];
    }

    /**
     * Delete Global.
     *
     * @param object $data
     * @return array
     */
    public function deleteGlobal($data)
    {
        $this->setting->delete(['key' => 'global_' . $data->key]);
    }

    /**
     * Get settings by param.
     *
     * @param array $param
     * @return array
     */
    public function getSettingsByParam($param)
    {
        return $this->setting->getByParam($param);
    }

    /**
     * Get the Column Per Slide.
     *
     * @return integer
     */
    public function getColumnPerSlide()
    {
        return ($this->hasProperty('column_per_slide')) ? $this->column_per_slide : self::column_per_slide;
    }

    /**
     * Get the Row Per Publisher.
     *
     * @return integer
     */
    public function getRowPerPublisher()
    {
        return ($this->hasProperty('row_per_publisher')) ? $this->row_per_publisher : self::row_per_publisher;
    }

    /**
     * Get the Advertiser Visible.
     *
     * @return integer
     */
    public function getAdvertiserVisible()
    {
        return ($this->hasProperty('advertiser_visible')) ? $this->advertiser_visible : self::advertiser_visible;
    }

    /**
     * method t
     * @return [type] [description]
     */
    public function showDefaultPointsPerPublisher($row = 1)
    {
        $points = ($this->getMaxRowsPublisher() + self::deducted_rows) - $row;
        if($this->useCustomOffer() && $this->isRowOfferExists($row)) $points = $this->getRowOffer($row, $points);
        return $points;
    }
     /**
     * [getMaxRowsPublisher] the max rows for each publisher columns
     * @return array
     */
    public function getMaxRowsPublisher()
    {
        return ($this->hasProperty('publisher_max_value')) ?  $this->publisher_max_value: self::max_rows;
    }

    /**
     * Check if using the custom offer.
     *
     * @return bolean
     */
    public function useCustomOffer()
    {
        if($this->hasProperty('use_custom_offer')) $data = unserialize($this->use_custom_offer);
        else $data = [];

        return(count($data) > 0 && @$data[0] == 'on') ? true : false;
    }

    /**
     * Check custom offer.
     *
     * @return bolean
     */
    public function isRowOfferExists($row)
    {
        return ($this->hasProperty('offer_row_' . $row))? true: false;
    }

    /**
     * Get custom offer by row nmber.
     *
     * @return bolean
     */
    public function getRowOffer($row, $default ='')
    {
        return ($this->hasProperty('offer_row_' . $row)) ? $this->{'offer_row_' . $row} : $default;
    }
}
