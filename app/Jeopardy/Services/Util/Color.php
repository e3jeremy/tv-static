<?php
namespace App\Jeopardy\Services\Util;

class Color
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    protected $selected_color = '';
    protected $default_color = '#242f35';
    protected $engageiq_color = '#00cc00';

    /**
     * Determine the color.
     *
     * @param string $name
     * @param string $color
     * @param string $value
     */
    public function determineTheSelectedColor ($name, $color, $value)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $name);
        $this->dataMust(['required' => true, 'string' => true], $color);
        $this->dataMust(['required' => true, 'string' => true], $value);

        if ($name == 'color') $color = $value;
        $this->selected_color = $color;
    }

    /**
     * Get the default color.
     *
     */
    public function default ()
    {
        return $this->default_color;
    }

    /**
     * Get the selected color.
     *
     */
    public function selected ()
    {
        return $this->selected_color;
    }

    /**
     * Get the color.
     *
     */
    public function get ()
    {
        return ($this->selected_color) ? $this->selected_color : $this->default();
    }

    /**
     * Set colors in for engageiq, which its not changeable
     *
     * @param string $color
     */
    public function setColorForEngageIq($color)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $color);

        if($color) $this->engageiq_color = $color;
    }

    /**
     * Get colors in for engageiq, which its not changeable
     *
     */
    public function getColorForEngageIq()
    {
        $this->engageiq_color;
    }

    /**
     * Set colors in for engageiq, which its not changeable
     *
     * @param string $color
     */
    public function setDefaultColor($color)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $color);

        if($color) $this->default_color = $color;
    }


    /**
     * Get colors in for engageiq, which its not changeable
     *
     */
    public function getDefaultColor()
    {
        $this->default_color;
    }

    /**
     * Set colors in relation to advertiser name
     *
     * @param array $default
     */
    public function setColors ($default)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $default);

        if(array_key_exists('engageiq_color', $default)) $this->setColorForEngageIq($default['engageiq_color']);
        if(array_key_exists('default_color', $default))  $this->setDefaultColor($default['default_color']);

        $colors = [];
        foreach (\Board::getAllAdvertisers() as $key) {
            foreach ($key as $value) {
                if(strtolower($value['advertiser_name']) == 'engageiq') {
                    $colors[strtolower($value['advertiser_name'])] = $this->engageiq_color;
                } else {
                    if($value['color']) $colors[strtolower($value['advertiser_name'])] = $value['color'];
                }
            }
        }
        if(!array_key_exists('engageiq', $colors)) $colors['engageiq'] = $this->engageiq_color;

        $this->colors = $colors;
    }

    /**
     * Get the colors
     *
     */
    public function getColors()
    {
        return $this->colors;
    }
}
?>
