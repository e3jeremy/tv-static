<?php
namespace App\Jeopardy\Services\Util;

use BoardSetting;

use App\Jeopardy\Services\Advertisers;

use App\Jeopardy\Repositories\Contracts\PointsMeterRepository;

class PointsMeterCalculator
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    /**
     * Variable to either update or not the points meter.
     *
     * @var string
     */
    protected $update_points_meter = 0;

    /**
     * Flag for updating the points if advertiser name(engageiq) was remove.
     * True means deduct points in points meter, vice versa.
     *
     * @var bolean
     */
    protected $deduct_when_engageiq_remove = false;

    /**
     * Variable container for updating the points.
     *
     * @var integer
     */
    protected $addend = '';

    /**
     * Get the addend variable.
     *
     * @param integer $id
     * @return array
     */
    public function getAddend()
    {
        return $this->addend;
    }

    /**
     * Get all points meter events records.
     *
     * @param string $name
     * @return array
     */
    public function deductPointsWhenEngageIqRemoved ($name)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $name);

        if($name == 'advertiser_name') return $this->deduct_when_engageiq_remove;
        else return true;
    }

    /**
     * Check if needed to update points meter.
     *
     * @param integer $id
     * @return array
     */
    public function checkIfNeeded2UpdatePointsMeter()
    {
        return $this->update_points_meter;
    }

    /**
     * Set data for updating points.
     *
     * @param array $existing_data
     * @param array $data
     * @return Bolean
     */
    public function setPointsMeterData($existing_data, $data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $existing_data);
        $this->dataMust(['required' => true, 'array' => true], $data);

        extract($data);
        if ($name === "advertiser_name")  $value = strtolower($value);

        if($name == 'points' || $name == 'advertiser_name') {
            if($name == 'points') $data = ['points' => (float)$value, 'advertiser_name' => $advertiser_name];
            if($name == 'advertiser_name') $data = ['points' => (float)$points, 'advertiser_name' => $value];
            // Start determining the correct (+-)points to add in the meter
            $this->compareAndSetDAta ($name, $existing_data, $data);
        }

        return $data;
    }

    /**
     * Set variables, compare existing data and submitted data
     *
     * @param string $name
     * @param array $existing_data | data saved in database
     * @param array $data | data that is submitted
     * @var string addend
     * @var integer update_points_meter
     * @return void
     */
    protected function compareAndSetDAta ($name, $existing_data, $data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $name);
        $this->dataMust(['required' => true, 'array' => true], $existing_data);
        $this->dataMust(['required' => true, 'array' => true], $data);

        // If existing advertiser name(saved in database) is engageiq
        if($this->isEngageIq($existing_data['advertiser_name'])) {
            // If submitted advertiser name(form data) is engageiq
            if($this->isEngageIq($data['advertiser_name'])) {
                // If the points value was change
                if($this->pointsWasChanged($data['points'], $existing_data['points'])) {
                    // Value to add;
                    $this->determineValue2Add ($data['points'], $existing_data['points']);
                    // Flag to update the points meter
                    $this->update_points_meter = 1;
                }
            }
            // If submitted advertiser name(form data) is not engageiq
            else {
                // Run this function if allowed
                if($this->deductPointsWhenEngageIqRemoved($name)) {
                    $this->addend = '-' . $existing_data['points'];
                    //Flag to update the points meter
                    $this->update_points_meter = 1;
                }
            }
        }
        // If existing advertiser name(saved in database) is not engageiq
        else {
            // If submitted advertiser name(form data) is engageiq
            if($this->isEngageIq($data['advertiser_name'])) {
                if($this->pointsWasChanged($data['points'], $existing_data['points'])) {
                    $this->addend = '+' . $existing_data['points'];
                    // Flag to update the points meter
                    $this->update_points_meter = 1;
                }
            }
            // If submitted advertiser name(form data) is not engageiq
            else {
                $this->addend = '';
                // Flag to update the points meter
                $this->update_points_meter = 0;
            }
        }
    }

    /**
     * Check if the advertiser name is engageiq.
     *
     * @param string $advertiser_name
     * @return bolean
     */
    protected function isEngageIq ($advertiser_name)
    {
        // Validate arguments and throw errors
       // $this->dataMust(['required' => true, 'string' => true], $advertiser_name);

        if(strtolower($advertiser_name) == 'engageiq') return true;
        return false;
    }

    /**
     * Check if points was change.
     *
     * @param integer $points
     * @param integer $existing_points
     * @return bolean
     */
    protected function pointsWasChanged ($points, $existing_points)
    {
        // Validate arguments and throw errors
        // $this->dataMust(['required' => true, 'integer' => true], $points);
        // $this->dataMust(['required' => true, 'integer' => true], $existing_points);

        if($points == $existing_points) {
            $this->addend = '';
            $this->update_points_meter = 0;

            return false;
        }
        return true;
    }

    /**
     * Compare the points of existing data and submitted data.
     *
     * @param integer $points
     * @param integer $existing_points
     * @return bolean
     */
    protected function submittedPointsIsGreaterThanExistingPoints ($points, $existing_points)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'integer' => true], $points);
        //$this->dataMust(['required' => true, 'integer' => true], $existing_points);

        if($points > $existing_points)  return true;
        return false;
    }

    /**
     * Determine the value will be added to points meter.
     *
     * @param integer $points
     * @param integer $existing_points
     */
    protected function determineValue2Add ($points, $existing_points)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'integer' => true], $points);
        //$this->dataMust(['required' => true, 'integer' => true], $existing_points);

        // If the submitted points value is greater than the saved points
        if($this->submittedPointsIsGreaterThanExistingPoints($points, $existing_points)) {
            $this->addend = '+' . ($points - $existing_points) ;
        }
        // If the submitted points value is less than the saved points
        else {
            $this->addend = '-' . ($existing_points - $points);
        }
    }
}
?>
