<?php
namespace App\Jeopardy\Services\Helpers;

trait DataHelper
{
    /**
     * Check type
     *
     * @param Mix $data
     * @return String type
     */
    protected function dataType($data)
    {
        return gettype($data);
    }
}
?>
