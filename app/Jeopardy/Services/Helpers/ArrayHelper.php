<?php
namespace App\Jeopardy\Services\Helpers;

trait ArrayHelper
{
    /**
     * container for unique tile placement nuber
     *
     * @var Array
     */
    protected $unique = [];

    /**
     * Set the placement value as index
     *
     * @param Array $data
     * @return Array
     */
    protected function fields2Index($fields = 'id', $data)
    {
        $new_data = [];
        foreach ($data as $key => $publisher) {
            if($this->ignoreIfDuplicate($publisher['tile_placement'])) continue;
            else $new_data[$publisher[$fields]] = $publisher;
        }

        return $new_data;
    }

    /**
     * Ignore duplicate because the first data is used in updating process
     *
     * @param array $data
     * @return array
     */
    protected function ignoreIfDuplicate($placement)
    {
        if(!in_array($placement, $this->unique)) {
            array_push($this->unique, $placement);
            return false;
        } else {
            return true;
        }
    }
}
?>
