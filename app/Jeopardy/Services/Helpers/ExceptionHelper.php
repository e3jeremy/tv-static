<?php
namespace App\Jeopardy\Services\Helpers;

use App\Exceptions\JeopardyException;

trait ExceptionHelper
{
    protected $default = [
        'required' => false,
        'string' => false,
        'integer' => false,
        'bolean' => false,
        'array' => false,
        'object' => false,
    ];

    protected $must = [];
    /**
     * Check type
     *
     * @param Mix $data
     * @return String type
     */
    protected function dataMust(Array $option, $data = '')
    {
        $this->must = array_merge($this->default, $option);
        $this->data = $data;
        $this->process();
    }

    /**
     * Check type
     *
     * @param Mix $data
     * @return String type
     */
    protected function process()
    {
        if($this->must['required']) {
            if($this->data == '') throw new JeopardyException('required');
        }
        if($this->must['string']) {
            if($this->dataType($this->data) != 'string') throw new JeopardyException('string_not_' .  $this->dataType($this->data));
        }
        if($this->must['integer']) {
            if($this->dataType($this->data) != 'integer') throw new JeopardyException('integer_not_' .  $this->dataType($this->data));
        }
        if($this->must['bolean']) {
            if($this->dataType($this->data) != 'bolean') throw new JeopardyException('bolean_not_' .  $this->dataType($this->data));
        }
        if($this->must['array']) {
            if($this->dataType($this->data) != 'array') throw new JeopardyException('array_not_' .  $this->dataType($this->data));
        }
        if($this->must['object']) {
            if($this->dataType($this->data) != 'object') throw new JeopardyException('object_not_' .  $this->dataType($this->data));
        }

        $error_id = (array_key_exists('error_id', $this->must)) ? $this->must['error_id'] : 'common';

        $msg = '';
        if(array_key_exists('error_msg', $this->must)) $msg = $this->must['error_msg'];
        if($error_id == 'specify_group') $msg = $this->must['recommended'];

        if($this->data == '') throw new JeopardyException($error_id, $msg);
    }
}
?>
