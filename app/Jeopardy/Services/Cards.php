<?php
namespace App\Jeopardy\Services;

use PointsMeter;
use BoardSetting;

use App\Jeopardy\Services\Advertisers;

use App\Jeopardy\Repositories\Contracts\CardRepository;

final class Cards
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    /**
     * instantiate and inject model
     *
     * @param Model $card
     * @return void
     */
    public function __construct(CardRepository $card)
    {
        $this->card = $card;
    }

    /**
     * Get all cards.
     *
     * @return array
     */
    public function selectAll()
    {
        return $this->card->all();
    }

    public function getCardEvents()
    {
        return PointsMeter::selectAllEventsWithFields(['id', 'card_id', 'card_title', 'card_description', 'instance']);
    }

    /**
     * Get random cards.
     *
     * @param array $data
     * @return array
     */
    public function getRandomCard()
    {
        $return = $this->card->randonCard();
        if (!$return['error']) {
            $data = PointsMeter::updateCardflippedStatus($return['card']);
            PointsMeter::createNewInstance($data);
            //$this->cardFlippedNotification();
            PointsMeter::getDrawnCardNotNotified();
        }
        //$return['allpoints'] = Advertisers::addAllPointsOfEngageIq();
        $return['flipped'] = false;
        $return['points'] = PointsMeter::getPoints();
        $return['goal'] = PointsMeter::getGoal();
        $return['percent'] = PointsMeter::getPercent();
        $return['instance'] = PointsMeter::getInstance(-1);

        return $return;
    }

    /**
     * Get card by id.
     *
     * @param object $data
     * @return array
     */
    public function GetDataByID($data)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'object' => true], $data);

        return $this->card->getData($data->id);
    }

    /**
     * Add new card.
     *
     * @param array $data
     * @return array
     */
    public function add($data)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'array' => true], $data);

        $this->card->add($data);

        return array(
                  'errors' => false,
                  'card_id' =>$this->card->getID()
                );
    }

    /**
     * Update card.
     *
     * @param array $data
     * @return array
     */
    public function update($data)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'array' => true], $data);

        $this->card->update($data);

        return array(
                  'errors' => false
                );
    }

    /**
     * Delete card.
     *
     * @param array $data
     * @return array
     */
    public function delete($data)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'array' => true], $data);

        $this->card->delete($data->id);

        return array(
                  'errors' => false
                );
    }
}
