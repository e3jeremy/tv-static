<?php

namespace App\Jeopardy\Repositories\Eloquent;

use Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Setting implements \App\Jeopardy\Repositories\Contracts\SettingRepository
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;
    use \App\Jeopardy\Repositories\Eloquent\Helpers\EloquentHelper;

    /**
     * Our Eloquent card model.
     *
     * @var object
     */
    protected $model;

    /**
     * Format for return.
     *
     * @var string
     */
    protected $return = 'array';

    /**
     * Setting our class $setting to the injected model
     *
     * @param Model $setting
     * @return EloquentRepository
     */
    public function __construct(Model $setting)
    {
          $this->model = $setting;
    }

    /**
     * Get all settings.
     *
     * @return mixed
     */
    public function all()
    {
        return $this->format($this->model->all());
    }

    /**
     * get setting with parameters.
     *
     * @param array $where
     * @return array
     */
    public function where($where)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $where);

		$result = $this->model->where($where)->get(['key' ,'value']);
        return $this->format($result->keyBy('key'));
    }

    /**
     * get setting by scope.
     *
     * @param integer $id
     * @return array
     */
    public function getByScope($id)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $id);

        return $this->format($this->model->where(array('scope' => 'publisher_' . $id))->get());
    }

    /**
     * get setting by key.
     *
     * @param string $key
     * @return array
     */
    public function getByKey($key)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $key);

        $return = $this->format($this->model->where(array('key' => $key))->get(['value']));
        if(isset($return[0])) return $return[0]['value'];
        return;
    }

    /**
     * get setting by param.
     *
     * @param array $param
     * @return array
     */
    public function getByParam($param)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $param);

        $return = $this->format($this->model->where($param)->get(['value']));
        if(isset($return[0])) return $return[0]['value'];
        return;
    }

    /**
     *  Add or update setting.
     *
     * @param array $data
     * @param string $scope
     * @return void
     */
    public function add($data, $scope)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);
        $this->dataMust(['required' => true, 'string' => true], $scope);

        if(!array_key_exists('use_custom_offer', $data)) {
            $data['use_custom_offer'] = [];
        }
        foreach ($data as  $key => $value) {
            if(is_array($value)) $value = serialize($value);
            if($key != '_token' && $value) {
                  $insert = [
                      'scope' => $scope,
                      'key' => $scope . '_' . $key,
                      'value' => $value
                  ];
                  $this->model->updateOrCreate([  'key' => $scope . '_' . $key], $insert);
            }
        }

        return $data;
    }

    /**
     *  Delete setting.
     *
     * @param array $param
     * @return void
     */
    public function delete($param)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $param);

        $data = $this->model->where($param)->delete();
    }
}
