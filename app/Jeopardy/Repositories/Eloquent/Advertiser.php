<?php

namespace App\Jeopardy\Repositories\Eloquent;

use Color;
//use PointsMeter;

use Eloquent\Collection;

class Advertiser implements \App\Jeopardy\Repositories\Contracts\AdvertiserRepository
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Repositories\Eloquent\Helpers\EloquentHelper;
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    /**
     * Our Eloquent advertiser model.
     *
     * @var object
     */
    protected $model;

    /**
     * Our Eloquent currrent advertiser model.
     *
     * @var object
     */
    protected $curr_model;

    /**
     * Variable to either update or not the points meter.
     *
     * @var string
     */
    public $update_points_meter = 0;

    /**
     * Format for return.
     *
     * @var string
     */
    protected $return = 'array';
    /**
     * website's folder name use in copying files
     */
    private $website;
    /**
    * Setting our class $advertiser to the injected model
    *
    * @param Model $advertiser
    * @return EloquentRepository
    */
    public function __construct(\Illuminate\Database\Eloquent\Model $advertiser)
    {
        $this->model = $this->curr_model = $advertiser;
        $this->website = getenv('WEBSITE_FOLDER');
    }

    /**
     * Get all advertiser.
     *
     * @return mixed
     */
    public function getAdvertiserData()
    {
        return $this->format($this->curr_model);
    }

    /**
     * Get all advertiser.
     *
     * @return mixed
     */
    public function getCurrentModel()
    {
        return $this->format($this->curr_model);
    }

    /**
     * Get advertiser data by id.
     *
     * @param integer $id
     * @return array
     */
    public function getData($id)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $id);

        $this->curr_model = $this->model->where(array('id' => $id))->first();
        return $this->format($this->curr_model);
    }

    /**
     * Get  advertiser by color.
     *
     * @return object
     */

    public function getByColor($color)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $color);

        $this->curr_model = $this->model->where('color', $color)->first();
        return $this->curr_model;
    }

    /**
     * Get advertisers.
     *
     * @param Array $param
     * @return mixed
     */
    public function get()
    {
        return $this->format($this->model->orderBy('tile_placement', 'ASC')->get());
    }

    /**
     * Get advertiser by group.
     *
     * @param Array $param
     * @return mixed
     */
    public function getByGroup($group)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $group);

        $advertiser = $this->model->orderBy('tile_placement', 'ASC')->get()->groupBy($group);

       // for ($i = 1; $i <= count($advertiser); $i ++) {
       //      if (array_key_exists($i, $advertiser)) {
       //          $group = $advertiser[$i]->keyBy('tile_placement');
       //          $advertiser[$i] = $group;
       //      }
           
            // $group = $advertiser[$i]->keyBy('tile_placement');
            // $advertiser[$i] = $group;
      // }

        foreach($advertiser as $key => $group) {
            $group = $group->keyBy('tile_placement');
            $advertiser[$key] = $group;
        }

        return $this->format($advertiser);
    }

    /**
     * Get advertiser by param.
     *
     * @param Array $param
     * @return mixed
     */
    public function getBy($param)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $param);

        $advertiser = [];
        if($param) $advertiser = $this->model->where($param)->orderBy('tile_placement', 'ASC')->get();

        return $this->format($advertiser);
    }

    public function getSum($advertiser_name)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $advertiser_name);

        return $this->model->where('advertiser_name', strtoupper($advertiser_name))
                           ->where('tile_placement', '>', 0)
                           ->sum('points');
    }

    /**
     * Get all advertiser.
     *
     * @return mixed
     */
    public function all()
    {
        return $this->format($this->model->orderBy('tile_placement', 'ASC')->get());
    }

    /**
     * Check if exists.
     *
     * @param integer $tile_placement
     * @param integer $publisher_id
     * @return Bolean
     */
    public function exists($tile_placement, $publisher_id)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $tile_placement);
        $this->dataMust(['required' => true, 'integer' => true], $publisher_id);

        if($this->curr_model = $this->model->where(array('tile_placement' => $tile_placement, 'publisher_id' => $publisher_id))->first()) return true;
        return false;
    }

    /**
     * Mass update color by advertiser name.
     *
     * @param string $advertiser_name
     * @return void
     */
    public function massUpdateColorByAdvertiserName($advertiser_name)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $advertiser_name);

        $this->model->where(['advertiser_name' => $advertiser_name])->update(['color' => Color::selected()]);
    }

    /**
     * Update advertiser whit data.
     *
     * @param string $name
     * @param mix $value
     * @param integer $points
     * @return void
     */
    public function update($name, $value, $points = '')
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $name);
        $this->dataMust(['required' => true], $value);

        $this->curr_model->$name = $value;
        $this->curr_model->color = Color::get();
        if($points !== '' && $name == 'advertiser_name') $this->curr_model->points = $points;
        $this->curr_model->push();
        /**
         * copy the advertiser logo to external source
         * @var file
         */
        if ($name === 'advertiser_logo') {
            try {
                copy(public_path() .'/'.$value, $this->website.'/'. $value);
            } catch (Exception $e) {
                echo $e;
            }
        }
        return $this->format($this->curr_model);
    }

    /**
     * Create new advertiser whit data.
     *
     * @param integer $publisher_id
     * @param integer $tile_placement
     * @param String $name
     * @param mix $value
     * @return Bolean
     */
    public function create($publisher_id, $tile_placement, $name, $value, $points = '')
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $publisher_id);
        $this->dataMust(['required' => true, 'integer' => true], $tile_placement);
        $this->dataMust(['required' => true, 'String' => true], $name);
        $this->dataMust(['required' => true], $value);

        $this->curr_model = $this->model;
        $this->curr_model->publisher_id = $publisher_id;
        $this->curr_model->tile_placement = $tile_placement;
        $this->curr_model->$name = $value;
        $this->curr_model->color = Color::get();
        if($points !== '' && $name == 'advertiser_name') $this->curr_model->points = $points;
        $this->curr_model->save();

        return $this->format($this->curr_model);
    }

    /**
     * Flags for updating the points if new.
     *
     * @param string $name
     * @param array $data
     * @return Bolean
     */
    public function isNew($name, $data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $name);
        $this->dataMust(['required' => true, 'array' => true], $data);

        extract($data);
        if($name == 'points' || $name == 'advertiser_name') {
            if($this->curr_model->points == 0
                && strtolower($data['advertiser_name']) == 'engageiq'
            ) return true;
            // else if($this->curr_model->points > 0
            //     && strtolower($data['advertiser_name']) == 'engageiq'
            //     && strtolower($this->curr_model->advertiser_name) != 'engageiq'
            //     && !PointsMeter::deductPointsWhenEngageIqRemoved($name)
            // ) return true;
            else return false;
        }

        return false;
    }

    /**
     * Get all advertiser data by placement.
     *
     * @param integer $placement
     * @return mixed
     */
    public function dataByPlacement($placement)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $placement);

        $this->curr_model = $this->model->where(array('tile_placement' => $placement))->get();
        return $this->format($this->curr_model);
    }

    /**
     * Get price by placement.
     *
     * @param integer $placement
     * @return mixed
     */
    public function getPriceByPlacement($placement)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $placement);

        $model = $this->dataByPlacement($placement);

        return $model[0]['price'];
    }

    /**
     * Reset all points when card is flip.
     *
     * @param integer $placement
     * @return mixed
     */
	public function resetAllPoints()
	{
		$this->model->update(['points' => 0]);
	}

    /**
     * Reset all points with advertiser name is "EngageIq" when card is flip.
     *
     * @param integer $placement
     * @return mixed
     */
	public function resetAllEngageIqPoints()
	{
		$this->model->where('advertiser_name', 'Engageiq')
					->update(['points' => 0]);
	}

    /**
     * Reset all points with advertiser name is not "EngageIq" when card is flip.
     *
     * @param integer $placement
     * @return mixed
     */
	public function resetAllNonEngageIqPoints()
	{
		$this->model->where('advertiser_name', '!=',  'Engageiq')
					->update(['points' => 0]);
	}
    /**
     * delete advertiser in a column
     */
    public function deleteAdvertiserInAColumn($advertiserId)
    {
        $advertiser = $this->model->find($advertiserId)->delete();
    }
}
