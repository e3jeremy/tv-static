<?php

namespace App\Jeopardy\Repositories\Eloquent;

use Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Card implements \App\Jeopardy\Repositories\Contracts\CardRepository
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Repositories\Eloquent\Helpers\EloquentHelper;
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    /**
     * Our Eloquent card model.
     *
     * @var object
     */
    protected $model;

    /**
     * Our Eloquent currrent advertiser model.
     *
     * @var object
     */
    protected $curr_model;

    /**
     * Format for return.
     *
     * @var string
     */
    protected $return = 'array';

    /**
     * Setting our class $card to the injected model
     *
     * @param Model $card
     * @return EloquentRepository
     */
    public function __construct(Model $card)
    {
        $this->model = $this->curr_model = $card;
    }

    /**
     * Get all cards.
     *
     * @return mixed
     */
    public function all()
    {
        return $this->format($this->model->orderBy('id', 'ASC')->get());
    }

    /**
     * Add new card.
     *
     * @param array $data
     * @return void
     */
    public function add($data)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'array' => true], $data);
        $this->curr_model = $this->model;
        $this->curr_model->title = $data['title'];
        $this->curr_model->description = $data['description'];
        $this->curr_model->save();
    }

    /**
     * Update card.
     *
     * @param array $data
     * @return void
     */
    public function update($data)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'array' => true], $data);

        $this->curr_model = $this->model->where(array('id' => $data['id']))->first();

        $this->curr_model->title = $data['title'];
        $this->curr_model->description = $data['description'];
        $this->curr_model->is_flipped = $data['is_flipped'];
        $this->curr_model->push();
    }

    /**
     * Delete card.
     *
     * @param integer $id
     * @return void
     */
    public function delete($id)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'integer' => true], $id);

        $this->model->where('id', $id)->delete();
    }

    /**
     * Get random card that is not yet flip.
     *
     * @param integer $id
     * @return mixed
     */
    public function randonCard()
    {
        $card = $this->model->where(array('is_flipped' => 0))->orderByRaw("RAND()")->first();
        if ($card) {
            $card->is_flipped = 1;
            $card->push();

            return array(
                'error' => false,
                'card' => $this->format($card)
            );
        } else {
            $this->model->where('is_flipped', '=', 1)->update(['is_flipped' => 0]);
            return array(
                'error' => true,
                'msg' => 'No more card to flip.'
            );
        }
    }
    /**
     * get the card that drawn today
     */
    public function getDrawnCardToday()
    {
        return $this->model->where('is_flipped', 1)->whereBetween('updated_at', array(Carbon::today(), Carbon::now()))->get();
    }
}
