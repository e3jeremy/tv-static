<?php

namespace App\Jeopardy\Repositories\Eloquent;

use Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class PointsMeter implements \App\Jeopardy\Repositories\Contracts\PointsMeterRepository
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;
    use \App\Jeopardy\Repositories\Eloquent\Helpers\EloquentHelper;

    /**
     * Our Eloquent card model.
     *
     * @var object
     */
    protected $model;

    /**
     * Format for return.
     *
     * @var string
     */
    protected $return = 'array';
    /**
     * PointsMeter $instance
     * @var resource
     */
    private $instance;
    /**
     * Setting our class $card to the injected model
     *
     * @param Model $pointsmeter
     * @return EloquentRepository
     */
    public function __construct(Model $pointsmeter)
    {
        $this->model = $pointsmeter;
    }

    /**
     * get events with parameters
     *
     * @param array $return
     */
    public function select($return)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $return);

       return $this->format($this->model->where(['card_flipped' => 1])->with('card')->get($return));
}

    /**
     * Get all events.
     *
     * @return mixed
     */
    public function all()
    {
        return $this->format($this->model->with('card')->orderBy('id', 'DESC')->get());
    }

    /**
     * Add new points meter event.
     *
     * @param array $where
     * @param array $data
     * @return void
     */
    public function add($where, $data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $where);
        $this->dataMust(['required' => true, 'array' => true], $data);

        $pointsmeter = $this->model->firstOrNew($where);
        if (!$this->pointsMeterExist($pointsmeter, $data)) $pointsmeter->fill($data)->save();
    }

    /**
     * Add new or update points meter.
     *
     * @param array $data
     * @return void
     */
    public function updateOrCreate($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        extract($data);
        $pointsmeter = $this->model->firstOrNew([ 'card_flipped' => 0]);
        if ($pointsmeter->exists) {
            $pointsmeter->goal = (float)$value;
            $pointsmeter->push();
            $insert = $pointsmeter;
        } else {
            $old = $this->model->where(['points' => (float)$points])->get();

            if(!$old->isEmpty()) $this->instance = $old->instance + 1;
            else $this->instance = 1;
            $insert = ['points' =>(float) $points, 'goal' => (float)$value, 'instance' => $this->instance];
            $this->add([ 'card_flipped' => 0], $insert);
        }
    }

    /**
     * Update card.
     *
     * @param array $data
     * @return void
     */
    public function updateTitle($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        $model = $this->model->where(array('id' => $data['id']))->first();

        $model->card_title = $data['card_title'];
        $model->push();
    }

    /**
     * Update card.
     *
     * @param array $data
     * @return void
     */
    public function updateDescription($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        $model = $this->model->where(array('id' => $data['id']))->first();

        $model->card_description = $data['card_description'];
        $model->push();
    }

    /**
     * Update specific points.
     *
     * @param mix $addend
     * @param object $obj
     * @return void
     */
    public function updatePoints($addend, $obj = '')
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true], $addend);

        if(!$obj) $pointsmeter = $this->getActive(false);
        else $pointsmeter = $obj;

        $points = $pointsmeter->points + (float) $addend;

        $pointsmeter->points = $points;
        $pointsmeter->incrementer++;
        $pointsmeter->push();
    }

    /**
     * Force update specific points.
     *
     * @param integer $total
     * @return void
     */
    public function updatePointsBrute($total)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true], $total);

        $pointsmeter = $this->getActive(false);
        if($pointsmeter) {
            $pointsmeter->points = $total;
            $pointsmeter->push();
        }
    }

    /**
     * Update if card is flip.
     *
     * @param array $card
     * @return void
     */
    public function updateCardflipped($card)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $card);

        $pointsmeter = $this->getActive(false);

        $pointsmeter->card_flipped = 1;
        $pointsmeter->card_id = $card['id'];
        $pointsmeter->card_title = $card['title'];
        $pointsmeter->card_description = $card['description'];
        $pointsmeter->push();

        return $this->format($pointsmeter);
    }

    /**
     * Get the points meter event.
     *
     * @param bolean $return_array
     * @return void
     */
    public function getActive($return_array = true)
    {
        $data = $this->model->where(['card_flipped' => 0])->first();
        if($data) {
            if($return_array) return $this->format($data);
            else return $data;
        }
        return;
    }

    /**
     * Update if exists.
     *
     * @param object $pointsmeter
     * @param array $data
     * @return void
     */
    protected function pointsMeterExist($pointsmeter, $data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'object' => true], $pointsmeter);
        $this->dataMust(['required' => true, 'array' => true], $data);

        if ($pointsmeter->exists) {
            $this->updatePoints('+' . $data['points'], $pointsmeter);
            return true;
        }
        return false;
    }

    /**
     * Delete card.
     *
     * @param integer $id
     * @return array
     */
    public function delete($id)
    {
        // Validate arguments and throw errors
        //$this->dataMust(['required' => true, 'integer' => true], $id);
        $model = $this->model->find($id);

        $model->delete();
    }
    /**
     * card drawn
     * @type array
     * @resource
     */
    public function getDrawnCard()
    {
        return $this->model->where('card_flipped', 1)->orderBy('instance', 'desc')->get();
    }
    /**
     * flipped card, but no notifiaction sent via email
     * @var resource
     */
    public function getDrawnCardNotNotified()
    {
        return $this->model->where('card_flipped', 1)->where('notified', 0)->value('id');
    }
    /**
     * update the notified field of points meter if card is drawn
     * @var resource
     */
    public function updateDrawnCardNotNotified($cardDrawnId)
    {
        $model = $this->model->find($cardDrawnId);
        $model->notified = 1;
        $model->save();
    }
    /**
     * flip the card if goal is reached
     * @var resource
     */
    public function flipTheCard($cardId)
    {
        $model = $this->model->find($cardId);
        $model->card_flipped = 1;
        $model->save();
    }
    /**
     * get the card that reached the goal but not flipped
     */
    public function getCardReachedGoal()
    {
        return $this->model->whereRaw('points >= goal')->whereRaw('card_flipped = 0')->value('id');
    }
}
