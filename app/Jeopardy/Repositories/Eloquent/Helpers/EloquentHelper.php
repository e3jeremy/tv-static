<?php
namespace App\Jeopardy\Repositories\Eloquent\Helpers;

trait EloquentHelper
{
    /**
     * Convert to eloquent object to base object, removing the eloquent methods.
     *
     * @param object $obj
     * @return object
     */
      protected function toBase($obj)
      {
          return $obj ? (object) $obj->toArray() : null;
      }

      /**
       * Convert to eloquent object to array.
       *
       * @param object $obj
       * @return array
       */
      protected function toArray($obj)
      {
          return  $obj->toArray();
      }

      /**
       * format object to either array or object base on saved parameter(this->return).
       *
       * @param object $obj
       * @return mixed
       */
      protected function format($data)
      {
          if ($this->return == 'array') {
              return $this->toArray($data);
          } elseif ($this->return == 'object_base') {
              return $this->toBase($data);
          } elseif ($this->return == 'object') {
              return $data;
          } else {
              return null;
          }
      }

      /**
       * Set format to either object or array.
       *
       * @param string $return
       * @return string
       */
      public function setReturn($return)
      {
          $this->return = $return;
      }

      /**
       * Get the model id.
       *
       * @return integer
       */
      public function getID()
      {
          return $this->curr_model->id;
      }

      /**
       * Get the model data by td.
       *
       * @param integer $id
       * @return mixed
       */
    public function getData($id)
    {
        return $this->format($this->curr_model->where(array('id' => $id))->get());
    }
}
