<?php

namespace App\Jeopardy\Repositories\Contracts;

/**
 * An interface....
 */
interface PublisherRepository
{
    public function all();

    public function getPublisherData();
        
    public function save($data);

    public function update($data);

    public function updatePlacement($data);

    public function delete($placement);

    public function getData($id);

    public function dataByPlacement($placement);

    public function getPriceByPlacement($placement);

    public function deletePublisher($publisherId);
}
