<?php
namespace App\Jeopardy\Facades;

use Illuminate\Support\Facades\Facade;

class CardsAndEventsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'card_and_event';
    }
}
?>
