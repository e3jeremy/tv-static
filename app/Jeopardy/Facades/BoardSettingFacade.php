<?php
namespace App\Jeopardy\Facades;

use Illuminate\Support\Facades\Facade;

class BoardSettingFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'board_settings';
    }
}
?>
