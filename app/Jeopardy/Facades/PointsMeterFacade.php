<?php
namespace App\Jeopardy\Facades;

use Illuminate\Support\Facades\Facade;

class PointsMeterFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'points_meter';
    }
}
?>
