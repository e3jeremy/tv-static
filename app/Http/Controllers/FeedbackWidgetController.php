<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeedbackWidgetController extends Controller
{
    private $repository;
    public function __construct(\App\Src\Widgets\Feedback\Repository\FeedbackRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return $this->repository->getAllFeedbackDataLimit50();
    }
}
