<?php

namespace App\Http\Controllers;

class DashboardController extends Controller
{
    public function index()
    {
        $pusherKey = config('broadcasting.connections.pusher.key');
        $pusherCluster = config('broadcasting.connections.pusher.options.cluster');
        $pusherSecret = config('broadcasting.connections.pusher.secret');
        $usingNodeServer = usingNodeServer();

        return view('dashboard')->with([
                'pusherKey' => $pusherKey,
                'pusherCluster' => $pusherCluster,
                'usingNodeServer' => $usingNodeServer
            ]
        );
    }
}
