<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminWidgetController extends Controller
{
    private $repository;
    public function __construct(\App\Src\Widgets\Admin\Repository\AdminRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return $this->repository->getAllAdminDataLimit10();
    }
}
