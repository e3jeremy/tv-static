<?php

namespace App\Traits;
use Auth;

trait HasRolesTrait 
{
	protected $user_roles = array();

	public function CheckIfUserHasRoles()
	{
		//Check if user is logged in
		if(Auth::check()) {
			$user = Auth::user();			
			// Get roles associated to the current user
			$user = $user->where(['id' => $user->id])->with(array('roles' => function($query){
										  $query->select('id', 'name');
									  }))
							  ->first(['id'])
							  ->toArray();

			// Push to array the roles name
			foreach($user['roles'] as $roles){
				array_push($this->user_roles, $roles['name']);
			}
		}
	}
	public function testCheckIfUserHasRoles()
	{
		//Check if user is logged in
		if(Auth::check()) {
			$user = Auth::user();			
			// Get roles associated to the current user
			$user = $user->where(['id' => $user->id])->with(array('roles' => function($query){
										  $query->select('id', 'name');
									  }))
							  ->first(['id'])
							  ->toArray();

			// Push to array the roles name
			foreach($user['roles'] as $roles){
				array_push($this->user_roles, $roles['name']);
			}
			print_r($this->user_roles);
		}
		else {
			echo "not logged";
		}
	}
}