<?php

namespace App\Components\Feedback;

use App\Events\Feedback\FeedbackUpdated;
use Illuminate\Console\Command;
use App\Src\Widgets\Feedback\Repository\FeedbackRepository;

class FetchFeedback extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'dashboard:feedback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Feedback Content.';
    /**
     * repository instance
     */
    private $repository;

    public function __construct(FeedbackRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileContent = $this->repository->getAllFeedbackDataLimit50();
        try {
            event(new FeedbackUpdated($fileContent));
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
