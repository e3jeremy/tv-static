<?php

namespace App\Src\Widgets\Feedback\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table ="feedbacks";
}
