<?php namespace App\Src\Widgets\Feedback\Repository;

use App\Src\Widgets\Feedback\Model\Feedback;

class FeedbackRepository
{
    private $feedback;
    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }
    public function getAllFeedbackDataLimit50()
    {
        return $this->feedback->orderBy('created_at', 'desc')->get();
    }
}
