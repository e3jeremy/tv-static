<?php

namespace App\Providers;

use App\Jeopardy\Services\Cards;
use App\Jeopardy\Services\Publishers;
use App\Jeopardy\Services\Advertisers;

use App\Jeopardy\Services\Util\Tile;
use App\Jeopardy\Services\Util\Color;
use App\Jeopardy\Services\Util\Board;
use App\Jeopardy\Services\Util\Settings;
use App\Jeopardy\Services\Util\CardsAndEvents;
use App\Jeopardy\Services\Util\PointsMeter as Meter;

use App\Jeopardy\Repositories\Eloquent\Card as CardEloquent;
use App\Jeopardy\Repositories\Eloquent\Setting as SettingEloquent;
use App\Jeopardy\Repositories\Eloquent\Publisher as PublisherEloquent;
use App\Jeopardy\Repositories\Eloquent\Advertiser as AdvertiserEloquent;
use App\Jeopardy\Repositories\Eloquent\PointsMeter as PointsMeterEloquent;

use App\Jeopardy\Entities\Card as CardEntity;
use App\Jeopardy\Entities\Publisher as PublisherEntity;
use App\Jeopardy\Entities\Advertiser as AdvertiserEntity;
use App\Jeopardy\Entities\PointsMeter as PointsMeterEntity;
use App\Jeopardy\Entities\BoardSetting as BoardSettingEntity;

use Illuminate\Support\ServiceProvider;

class JeopardyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // View
        \Blade::extend(function($value) {
            return preg_replace('/(\s*)\{\? (.+) \?\}(\s*)/', '$1<?php $2; ?>$3', $value);
        });
        \Blade::extend(function($value) {
            return preg_replace("/(\s*)\{\? (\s*)/", "$1<?php $3", $value);
        });
        \Blade::extend(function($value) {
            return preg_replace("/(\s*)\{\?\r(\s*)/", "$1<?php\r$3", $value);
        });
        \Blade::extend(function($value) {
            return preg_replace("/(\s*)\?\}\r(\s*)/", "$1 ?>\r$3", $value);
        });

        //Interfaces
        $this->app->bind('App\Jeopardy\Repositories\Contracts\PublisherRepository', function($app) {
            return new PublisherEloquent(new PublisherEntity);
        });
        $this->app->bind('App\Jeopardy\Repositories\Contracts\AdvertiserRepository', function($app) {
            return new AdvertiserEloquent(new AdvertiserEntity);
        });
        $this->app->bind('App\Jeopardy\Repositories\Contracts\CardRepository', function($app) {
            return new CardEloquent(new CardEntity);
        });
        $this->app->bind('App\Jeopardy\Repositories\Contracts\SettingRepository', function($app) {
            return new SettingEloquent(new BoardSettingEntity);
        });
        $this->app->bind('App\Jeopardy\Repositories\Contracts\PointsMeterRepository', function($app) {
            return new PointsMeterEloquent(new PointsMeterEntity);
        });

        //Facades
        $this->app->bind('color', function() {
            return new Color;
        });
        $this->app->bind('board', function() {
            return new Board;
        });

        $this->app->bind('tile', function() {
            return new Tile;
        });

        $this->app->bind('card_and_event', function() {
            return new CardsAndEvents;
        });

        $this->app->bind('board_settings', function() {
            return new Settings(new SettingEloquent(new BoardSettingEntity));
        });

        $this->app->bind('points_meter', function() {
            return new Meter( new PointsMeterEloquent(new PointsMeterEntity));
        });


    }
}
