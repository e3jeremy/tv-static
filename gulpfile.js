/**
* Custom gulp file for TVPROJECT
*        
#   /$$$$$$$$ /$$    /$$ /$$$$$$$  /$$$$$$$   /$$$$$$     /$$$$$ /$$$$$$$$  /$$$$$$  /$$$$$$$$
#  |__  $$__/| $$   | $$| $$__  $$| $$__  $$ /$$__  $$   |__  $$| $$_____/ /$$__  $$|__  $$__/
#     | $$   | $$   | $$| $$  \ $$| $$  \ $$| $$  \ $$      | $$| $$      | $$  \__/   | $$   
#     | $$   |  $$ / $$/| $$$$$$$/| $$$$$$$/| $$  | $$      | $$| $$$$$   | $$         | $$   
#     | $$    \  $$ $$/ | $$____/ | $$__  $$| $$  | $$ /$$  | $$| $$__/   | $$         | $$   
#     | $$     \  $$$/  | $$      | $$  \ $$| $$  | $$| $$  | $$| $$      | $$    $$   | $$   
#     | $$      \  $/   | $$      | $$  | $$|  $$$$$$/|  $$$$$$/| $$$$$$$$|  $$$$$$/   | $$   
#     |__/       \_/    |__/      |__/  |__/ \______/  \______/ |________/ \______/    |__/   
#                                                                                             
#                                                                                             
#    
 */
const elixir = require('laravel-elixir');

/**
 * minify css
 *
 */
const minifyCss = require('gulp-minify-css');
/**
 * uglify js
 *
 */
const uglify = require('gulp-uglify');
/**
 * rename scripts.js,css.css to .min.js,.min.css
 * @type {[type]}
 */
const rename = require('gulp-rename');
/**
 * the notifier
 */
const notify = require("gulp-notify");
/**
 * laravel vue js
 */

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// elixir(mix => {
//     mix
//         .sass('app.scss')
//         .webpack('app.js');

//     mix
//         .copy('resources/assets/bower_components/bootstrap/dist/fonts', 'public/build/fonts');

//     mix
//         .styles([
//             '../bower_components/bootstrap/dist/css/bootstrap.css'
//         ], 'public/css/vendor.css');

//     mix
//         .version(['css/vendor.css', 'js/app.js']);
// });

//*** CSS & JS minify task
gulp.task('minify', function () {
    /**
     * run this task, gulp minify inside your project
     * you can add your css and js files here to be minified by this script
     * if you are updating .css or .js files, just delete the .min.css/.min.js before running this task
     */
    // css minify

       // gulp.src(['public/css/jeopardy/*.css', '!public/css/jeopardy/*.min.css']).pipe(minifyCss()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/css/jeopardy/'));
       // gulp.src(['public/css/partA/*.css','!public/css/partA/*.min.css']).pipe(minifyCss()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/css/partA/'));
       // gulp.src(['public/css/rotator/*.css','!public/css/rotator/*.min.css']).pipe(minifyCss()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/css/rotator/'));
        //gulp.src(['public/css/*.css', '!public/css/*.min.css']).pipe(minifyCss()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/css/')).pipe(notify({message: "ohh common baby lets go party ah ahhhhh ha my barbie girl on a barbie world @<%=options.date %> @<%=options.title %>",templateOptions:{date: new Date()},title: "TVPROJECT NOTIFIER"}));

    //js minify
        gulp.src(['public/js/*.js','!public/js/*.min.js']).pipe(uglify()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/js/'));
        gulp.src(['public/js/jeopardy/*.js','!public/js/jeopardy/*.min.js']).pipe(uglify()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/js/jeopardy/'));
        //gulp.src(['public/js/partA/user/*.js','!public/js/partA/user/*.min.js']).pipe(uglify()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/js/partA/user/'));
        gulp.src(['public/js/partA/*.js','!public/js/partA/*.min.js']).pipe(uglify()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/js/partA/'));
        //gulp.src(['public/js/rotator/*.js','!public/js/rotator/*.min.js']).pipe(uglify()).pipe(rename({suffix: '.min'})).pipe(gulp.dest('public/js/rotator/'));

});